package org.eclipse.gemoc.execution.commons.commands;

import java.io.Serializable;

public class SetVariableCommand  implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	public String variableQualifiedName;
	public Object newValue;
	
	public SetVariableCommand(String variableQN, Object newVal) {
		variableQualifiedName = variableQN;
		newValue = newVal;
	}

}
