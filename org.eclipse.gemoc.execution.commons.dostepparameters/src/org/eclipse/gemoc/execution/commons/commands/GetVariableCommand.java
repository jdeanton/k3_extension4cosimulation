package org.eclipse.gemoc.execution.commons.commands;

import java.io.Serializable;

public class GetVariableCommand  implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	public String variableQualifiedName;
	
	public GetVariableCommand(String variableQN) {
		variableQualifiedName = variableQN;
	}
}
