package org.eclipse.gemoc.execution.commons.commands;

import java.io.Serializable;

public class StopCondition implements Serializable{
	
	private static final long serialVersionUID = 1L;
		public StopReason stopReason;
//		public Object caller;
//		public Object propContainer;
//		public StepCommand command;
		public String objectQualifiedName;
		public String propertyName;
		public double timeValue;
		
		public StopCondition(StopReason reason, /*Object c, Object propCont, StepCommand com,*/ String objectQN, String propName, double time) {
			this.stopReason = reason;
//			this.caller =c;
//			this.propContainer = propCont;
//			this.command = com;
			this.objectQualifiedName = objectQN;
			this.propertyName = propName;
			this.timeValue = time;
		}
	}