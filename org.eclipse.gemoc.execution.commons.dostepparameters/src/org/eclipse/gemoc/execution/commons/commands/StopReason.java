package org.eclipse.gemoc.execution.commons.commands;

import java.io.Serializable;

public enum StopReason implements Serializable { UPDATE, READYTOREAD, TIME, LOGICALSTEP, DEBUGSTEP, EVENT}
	