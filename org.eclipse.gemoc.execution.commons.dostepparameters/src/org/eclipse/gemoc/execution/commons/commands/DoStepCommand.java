package org.eclipse.gemoc.execution.commons.commands;

import java.io.Serializable;

import org.eclipse.gemoc.execution.commons.predicates.CoordinationPredicate;

public class DoStepCommand implements Serializable{
	
	private static final long serialVersionUID = 1L;
	public CoordinationPredicate predicate;
	
	public DoStepCommand(CoordinationPredicate pred) {
		predicate = pred;
	}

}
