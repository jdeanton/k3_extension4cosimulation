package org.eclipse.gemoc.execution.commons.predicates;


import org.eclipse.emf.ecore.EObject;
import org.eclipse.gemoc.execution.commons.commands.StopReason;

public class DebugPredicate extends CoordinationPredicate{
	
	private static final long serialVersionUID = 1L;
	
	public DebugPredicate() {
	}

	@Override
	public boolean contains(StopReason predType, EObject caller, String callerName, String eventTypeName) {
		return predType == StopReason.DEBUGSTEP;
	}

	@Override
	public TemporalPredicate getTemporalPredicate() {
		return null;
	}
	
	@Override
	public LogicalStepPredicate getLogicalStepPredicate() {
		return null;
	}
	
}