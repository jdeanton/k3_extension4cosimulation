package org.eclipse.gemoc.execution.commons.predicates;


import org.eclipse.emf.ecore.EObject;
import org.eclipse.gemoc.execution.commons.commands.StopReason;
import org.eclipse.gemoc.execution.commons.helpers.EcoreQNHelper;

public class VariablePredicate extends CoordinationPredicate{
	
	private static final long serialVersionUID = 1L;
	public String objectQName;
	public String fieldName;
	
	public VariablePredicate(String fieldName, String objQN) {
		objectQName = objQN;
		this.fieldName = fieldName;
	}

	@Override
	public boolean contains(StopReason predType, EObject caller, String className, String propertyName) {
		if (caller == null) return false;
		String callerQName = EcoreQNHelper.getQualifiedName(caller);
		return (objectQName+"::"+fieldName).compareTo(callerQName+"::"+propertyName) == 0;
	}

	@Override
	public TemporalPredicate getTemporalPredicate() {
		return null;
	}
	
	@Override
	public LogicalStepPredicate getLogicalStepPredicate() {
		return null;
	}
	
}