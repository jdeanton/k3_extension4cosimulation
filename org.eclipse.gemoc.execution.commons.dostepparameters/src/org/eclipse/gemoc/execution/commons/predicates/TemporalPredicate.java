package org.eclipse.gemoc.execution.commons.predicates;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.gemoc.execution.commons.commands.StopReason;

public class TemporalPredicate extends CoordinationPredicate{
	
	private static final long serialVersionUID = 1L;
	public int deltaT;
	public TemporalPredicate(int deltaT) {
		this.deltaT = deltaT;
	}
	@Override
	public boolean contains(StopReason predType, EObject caller, String className, String propertyName) {
		return predType == StopReason.TIME;
	}
	@Override
	public TemporalPredicate getTemporalPredicate() {
		return this;
	}
	
	@Override
	public LogicalStepPredicate getLogicalStepPredicate() {
		return null;
	}
}