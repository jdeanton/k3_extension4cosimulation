package org.eclipse.gemoc.execution.commons.predicates;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.gemoc.execution.commons.commands.StopReason;

public class ReadyToReadPredicate extends VariablePredicate{
	
	private static final long serialVersionUID = 1L;

	public ReadyToReadPredicate(String fieldName, String objQN) {
		super(fieldName, objQN);
	}
	
	@Override
	public boolean contains(StopReason predType, EObject caller, String className, String propertyName) {
		return predType == StopReason.READYTOREAD && super.contains(predType, caller, className, propertyName);
	}
	
}

