package org.eclipse.gemoc.execution.commons.predicates;

import java.io.Serializable;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.gemoc.execution.commons.commands.StopReason;

public abstract class CoordinationPredicate  implements Serializable{
	
	private static final long serialVersionUID = 1L;
	public boolean isVerified = false;

	public abstract boolean contains(StopReason predType, EObject caller, String className, String propertyName);

	public abstract TemporalPredicate getTemporalPredicate();
	public abstract LogicalStepPredicate getLogicalStepPredicate();
}
