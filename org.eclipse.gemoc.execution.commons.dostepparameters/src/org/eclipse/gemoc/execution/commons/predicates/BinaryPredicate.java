package org.eclipse.gemoc.execution.commons.predicates;

import java.io.Serializable;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.gemoc.execution.commons.commands.StopReason;

public class BinaryPredicate extends CoordinationPredicate  implements Serializable{
	
	private static final long serialVersionUID = 1L;
	CoordinationPredicate leftPredicate;
	CoordinationPredicate rightPredicate;
	public enum BooleanBinaryOperator{
		AND, OR;
	}
	BooleanBinaryOperator operator;
	
	public BinaryPredicate(CoordinationPredicate l, CoordinationPredicate r) {
		leftPredicate = l;
		rightPredicate = r;
		operator = BooleanBinaryOperator.OR;
	}
	
	public BinaryPredicate(CoordinationPredicate l, CoordinationPredicate r, BooleanBinaryOperator op) {
		leftPredicate = l;
		rightPredicate = r;
		operator = op;
	}

	@Override
	public boolean contains(StopReason predType, EObject caller, String className, String propertyName) {
		return leftPredicate.contains(predType, caller, className, propertyName) || rightPredicate.contains(predType, caller, className, propertyName);
	}

	@Override
	public TemporalPredicate getTemporalPredicate() {
		TemporalPredicate lp = leftPredicate.getTemporalPredicate();
		TemporalPredicate rp = rightPredicate.getTemporalPredicate();
		if (lp != null) {
			return lp;
		}
		return rp; //may be null if not TemporalPredicate are defined
	}
	
	@Override
	public LogicalStepPredicate getLogicalStepPredicate() {
		LogicalStepPredicate lp = leftPredicate.getLogicalStepPredicate();
		LogicalStepPredicate rp = rightPredicate.getLogicalStepPredicate();
		if (lp != null) {
			return lp;
		}
		return rp; //may be null if not LogicalStepPredicate are defined
	}
	
}