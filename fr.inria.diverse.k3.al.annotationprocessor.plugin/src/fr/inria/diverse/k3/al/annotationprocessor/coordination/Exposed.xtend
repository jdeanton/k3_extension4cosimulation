package fr.inria.diverse.k3.al.annotationprocessor.coordination

import java.lang.annotation.ElementType
import java.lang.annotation.Retention
import java.lang.annotation.RetentionPolicy
import java.lang.annotation.Target

@Retention(RetentionPolicy::RUNTIME)
//@Active(typeof(AspectProcessor)) 
//@Target(ElementType.FIELD)
annotation Exposed {
//	Direction direction;
//	enum Direction{
//		IN,
//		OUT,
//		INOUT;
//	}
//	
}
