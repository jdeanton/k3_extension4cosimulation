package fr.inria.diverse.k3.al.annotationprocessor.coordination

import java.lang.annotation.Retention
import java.lang.annotation.RetentionPolicy

@Retention(RetentionPolicy::RUNTIME)
//@Active(typeof(AspectProcessor)) 
//@Target(ElementType.FIELD)
annotation Time {
	
}
