/*******************************************************************************
 * Copyright (c) 2020 I3S/Inria and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     I3S/Inria Kairos - initial API and implementation
 *******************************************************************************/
 package fr.inria.diverse.k3.al.annotationprocessor.coordination

import org.eclipse.emf.ecore.EObject

interface ICoordinationManager extends fr.inria.diverse.k3.al.annotationprocessor.stepmanager.IStepManager { 

	def void readyToRead(EObject caller, Object propContainer, fr.inria.diverse.k3.al.annotationprocessor.stepmanager.StepCommand command, String className, String propertyName);

	def void justUpdated(EObject caller, Object propContainer, fr.inria.diverse.k3.al.annotationprocessor.stepmanager.StepCommand command, String className, String propertyName);

	def void timePassed(EObject caller, Object propContainer, fr.inria.diverse.k3.al.annotationprocessor.stepmanager.StepCommand command, String className, double value);

}
