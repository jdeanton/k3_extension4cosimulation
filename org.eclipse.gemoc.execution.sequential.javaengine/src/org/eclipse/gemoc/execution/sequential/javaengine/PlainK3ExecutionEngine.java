/*******************************************************************************
 * Copyright (c) 2016, 2020 Inria and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Inria - initial API and implementation
 *     I3S Kairos - add stepping API
 *******************************************************************************/
package org.eclipse.gemoc.execution.sequential.javaengine;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Semaphore;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.emf.transaction.impl.InternalTransactionalEditingDomain;
import org.eclipse.gemoc.execution.commons.commands.DoStepCommand;
import org.eclipse.gemoc.execution.commons.commands.GetVariableCommand;
import org.eclipse.gemoc.execution.commons.commands.SetVariableCommand;
import org.eclipse.gemoc.execution.commons.commands.StopCommand;
import org.eclipse.gemoc.execution.commons.commands.StopCondition;
import org.eclipse.gemoc.execution.commons.commands.StopReason;
import org.eclipse.gemoc.execution.commons.helpers.EcoreQNHelper;
import org.eclipse.gemoc.execution.commons.predicates.CoordinationPredicate;
import org.eclipse.gemoc.execution.commons.predicates.TemporalPredicate;
import org.eclipse.gemoc.executionframework.engine.commons.GenericModelExecutionContext;
import org.eclipse.gemoc.executionframework.engine.commons.K3DslHelper;
import org.eclipse.gemoc.executionframework.engine.commons.sequential.ISequentialRunConfiguration;
import org.eclipse.gemoc.executionframework.engine.core.AbstractCommandBasedSequentialExecutionEngine;
import org.eclipse.gemoc.executionframework.engine.core.EngineStoppedException;
import org.eclipse.jdt.core.IPackageFragmentRoot;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.search.IJavaSearchConstants;
import org.eclipse.jdt.core.search.IJavaSearchScope;
import org.eclipse.jdt.core.search.SearchEngine;
import org.eclipse.jdt.core.search.SearchMatch;
import org.eclipse.jdt.core.search.SearchParticipant;
import org.eclipse.jdt.core.search.SearchPattern;
import org.eclipse.jdt.core.search.SearchRequestor;
import org.osgi.framework.Bundle;

import com.google.common.util.concurrent.AtomicDouble;

import fr.inria.diverse.k3.al.annotationprocessor.coordination.ICoordinationManager;
import fr.inria.diverse.k3.al.annotationprocessor.coordination.Input;
import fr.inria.diverse.k3.al.annotationprocessor.coordination.Output;
import fr.inria.diverse.k3.al.annotationprocessor.coordination.Time;
import fr.inria.diverse.k3.al.annotationprocessor.stepmanager.StepCommand;
import fr.inria.diverse.k3.al.annotationprocessor.stepmanager.StepManagerRegistry;
import fr.inria.diverse.melange.adapters.EObjectAdapter;

/**
 * Implementation of the GEMOC Execution engine dedicated to run Kermeta 3
 * operational semantic
 * 
 * @author Didier Vojtisek<didier.vojtisek@inria.fr>
 * @author Julien Deantoni<julien.deantoni@univ-cotedazur.fr>
 */
public class PlainK3ExecutionEngine extends AbstractCommandBasedSequentialExecutionEngine<GenericModelExecutionContext<ISequentialRunConfiguration>, ISequentialRunConfiguration>
		implements ICoordinationManager {

	private Method initializeMethod;
	private List<Object> initializeMethodParameters;
	private Method entryPointMethod;
	private List<Object> entryPointMethodParameters;
	private Class<?> entryPointClass;
	private String languageName = "";
	protected EObject root = null;

	@Override
	public String engineKindName() {
		return "GEMOC Kermeta Sequential Engine";
	}

	/**
	 * Constructs a PlainK3 execution engine using an entry point (~ a main
	 * operation) The entrypoint will register itself as a StepManager into the K3
	 * step manager registry, and unregister itself at the end. As a StepManager,
	 * the PlainK3ExecutionEngine will receive callbacks through its "executeStep"
	 * operation.
	 */
	@Override
	protected void prepareEntryPoint(GenericModelExecutionContext<ISequentialRunConfiguration> executionContext) {
		/*
		 *store the language name 
		 */
		languageName = executionContext.getRunConfiguration().getLanguageName();
		
		/*
		 * Get info from the RunConfiguration
		 */
		String entryPoint = executionContext.getRunConfiguration().getExecutionEntryPoint();
		String mainModelElementURI = executionContext.getRunConfiguration().getModelEntryPoint();

		/*
		 * Find the entry point in the workspace
		 */
		final String prefix = "public static void ";
		int startName = prefix.length();
		int endName = entryPoint.lastIndexOf("(");
		String methodFullName = entryPoint.substring(startName, endName);

		String aspectClassName = methodFullName.substring(0, methodFullName.lastIndexOf("."));
		String methodName = methodFullName.substring(methodFullName.lastIndexOf(".") + 1);

		Bundle bundle = findBundle(executionContext, aspectClassName);
		if (bundle == null)
			throw new RuntimeException("Could not find bundle for language \""
					+ executionContext.getRunConfiguration().getLanguageName() + "\"");

		// search the class
		try {
			entryPointClass = bundle.loadClass(aspectClassName);
		} catch (ClassNotFoundException e) {
			String bundleName = bundle.getHeaders().get("Bundle-Name");
			e.printStackTrace();
			throw new RuntimeException(
					"Could not find class " + executionContext.getRunConfiguration().getExecutionEntryPoint()
							+ " in bundle " + bundleName + ".");
		}

		// search the method
		this.entryPointMethodParameters = new ArrayList<>();
		root = executionContext.getResourceModel().getEObject(mainModelElementURI);
		if (root instanceof EObjectAdapter) {
			entryPointMethodParameters.add(((EObjectAdapter<?>) root).getAdaptee());
		} else {
			entryPointMethodParameters.add(root);
		}
		try {
			this.entryPointMethod = K3DslHelper.findMethod(entryPointClass, root, methodName);
		} catch (Exception e) {
			String msg = "There is no \"" + methodName + "\" method in " + entryPointClass.getName()
					+ " with first parameter able to handle " + entryPointMethodParameters.get(0).toString();
			msg += " from " + ((EObject) entryPointMethodParameters.get(0)).eClass().getEPackage().getNsURI();
			Activator.error(msg, e);
			throw new RuntimeException("Could not find method main with correct parameters.");
		}
	}

	@Override
	protected void prepareInitializeModel(GenericModelExecutionContext<ISequentialRunConfiguration> executionContext) {

		// try to get the initializeModelRunnable
		String modelInitializationMethodQName = executionContext.getRunConfiguration().getModelInitializationMethod();
		if (!modelInitializationMethodQName.isEmpty()) {
			// the current system supposes that the modelInitialization method
			// is in the same class as the entry point
			String modelInitializationMethodName = modelInitializationMethodQName
					.substring(modelInitializationMethodQName.lastIndexOf(".") + 1);
			boolean isListArgs = false;
			boolean isEListArgs = false;
			boolean isFound = false;
			try {
				Class<?>[] modelInitializationParamType = new Class[] {
						entryPointMethodParameters.get(0).getClass().getInterfaces()[0], String[].class };
				initializeMethod = entryPointClass.getMethod(modelInitializationMethodName,
						modelInitializationParamType);
				isListArgs = false; // this is a java array
				isFound = true;
			} catch (Exception e) {

			}
			if (!isFound) {
				try {
					Class<?>[] modelInitializationParamType = new Class[] {
							entryPointMethodParameters.get(0).getClass().getInterfaces()[0], List.class };
					initializeMethod = entryPointClass.getMethod(modelInitializationMethodName,
							modelInitializationParamType);
					isListArgs = true; // this is a List
					isFound = true;
				} catch (Exception e) {

				}
			}
			if (!isFound) {
				try {
					Class<?>[] modelInitializationParamType = new Class[] {
							entryPointMethodParameters.get(0).getClass().getInterfaces()[0], EList.class };
					this.initializeMethod = entryPointClass.getMethod(modelInitializationMethodName,
							modelInitializationParamType);
					isEListArgs = true; // this is an EList
				} catch (Exception e) {
					String msg = "There is no \"" + modelInitializationMethodName + "\" method in "
							+ entryPointClass.getName() + " with first parameter able to handle "
							+ entryPointMethodParameters.get(0).toString();
					msg += " and String[] or List<String> or EList<String> args as second parameter";
					msg += " from " + ((EObject) entryPointMethodParameters.get(0)).eClass().getEPackage().getNsURI();
					Activator.error(msg, e);
					// ((EObject)parameters.get(0)).eClass().getEPackage().getNsURI()
					throw new RuntimeException(
							"Could not find method " + modelInitializationMethodName + " with correct parameters.");
				}
			}
			final boolean finalIsListArgs = isListArgs;
			final boolean finalIsEListArgs = isEListArgs;
			this.initializeMethodParameters = new ArrayList<>();
			initializeMethodParameters.add(entryPointMethodParameters.get(0));
			if (finalIsListArgs) {
				final ArrayList<Object> modelInitializationListParameters = new ArrayList<>();
				for (String s : executionContext.getRunConfiguration().getModelInitializationArguments()
						.split("\\r?\\n")) {
					modelInitializationListParameters.add(s);
				}
				initializeMethodParameters.add(modelInitializationListParameters);
			} else if (finalIsEListArgs) {
				final EList<Object> modelInitializationListParameters = new BasicEList<>();
				for (String s : executionContext.getRunConfiguration().getModelInitializationArguments()
						.split("\\r?\\n")) {
					modelInitializationListParameters.add(s);
				}
				initializeMethodParameters.add(modelInitializationListParameters);
			} else {
				initializeMethodParameters
						.add(executionContext.getRunConfiguration().getModelInitializationArguments().split("\\r?\\n"));
			}
		}
		
		populateVariableMapFromAnnotations();
		
	}

	
	/**
	 * this method go through all the model and its aspects and populate the {@link #qualifiedNameToEObject} map with Variables annotated as @Input or @Output
	 */
	private void populateVariableMapFromAnnotations() {
		TreeIterator<EObject> contentIt = root.eAllContents();
		while(contentIt.hasNext()) {
			EObject eo = contentIt.next();
			List<Class<?>> aspects = K3DslHelper.getAspectsOn(languageName, eo.getClass());
			for(Class<?> aspect : aspects) {
				try {
					Class<?> propertyClass = this.getClass().getClassLoader().loadClass(aspect.getTypeName()+aspect.getSimpleName()+"Properties");
					for (Field f : propertyClass.getFields()) {
						for (Method m : aspect.getMethods()) {
							if (m.getName().compareTo(f.getName()) == 0) {
								if (m.isAnnotationPresent(Input.class)) {
									qualifiedNameToEObject.putIfAbsent(EcoreQNHelper.getQualifiedName(eo)+"::"+f.getName(), eo);
									if (m.getParameterCount() == 2) {
										qualifiedNameToMethod.putIfAbsent(EcoreQNHelper.getQualifiedName(eo)+"::"+f.getName()+"::set", m);
									}
								}
								if (m.isAnnotationPresent(Output.class)) {
									qualifiedNameToEObject.putIfAbsent(EcoreQNHelper.getQualifiedName(eo)+"::"+f.getName(), eo);
									if (m.getParameterCount() == 1) {
										qualifiedNameToMethod.putIfAbsent(EcoreQNHelper.getQualifiedName(eo)+"::"+f.getName()+"::get", m);
									}
								}
								if (m.isAnnotationPresent(Time.class)) {
									try {
										timeObject = f.get(eo); 
										timeType = f.getType();
									} catch (IllegalArgumentException | IllegalAccessException e) {
										e.printStackTrace();
									}
								}
							}
						}
					}
				} catch (ClassNotFoundException | SecurityException e1) {
					e1.printStackTrace();
				}
			}
		}
			
	}

	/**
	 * Invoke the initialize method
	 */
	private void callInitializeModel() {
		try {
			initializeMethod.invoke(null, initializeMethodParameters.toArray());
		} catch (EngineStoppedException stopExeception) {
			// not really an error, simply forward the stop exception
			throw stopExeception;
		} catch (java.lang.reflect.InvocationTargetException ite) {
			// not really an error, simply forward the stop exception
			if (ite.getCause() instanceof EngineStoppedException) {
				throw (EngineStoppedException) ite.getCause();
			} else {
				throw new RuntimeException(ite);
			}
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	protected void initializeModel() {
		if (initializeMethod != null) {
			StepManagerRegistry.getInstance().registerManager(PlainK3ExecutionEngine.this);
			try {
				final boolean isStepMethod = initializeMethod
						.isAnnotationPresent(fr.inria.diverse.k3.al.annotationprocessor.Step.class);
				if (!isStepMethod) {
					fr.inria.diverse.k3.al.annotationprocessor.stepmanager.StepCommand command = new fr.inria.diverse.k3.al.annotationprocessor.stepmanager.StepCommand() {
						@Override
						public void execute() {
							callInitializeModel();
						}
					};
					fr.inria.diverse.k3.al.annotationprocessor.stepmanager.IStepManager stepManager = PlainK3ExecutionEngine.this;
					stepManager.executeStep(entryPointMethodParameters.get(0), command, entryPointClass.getName(),
							initializeMethod.getName());
				} else {
					callInitializeModel();
				}
			} finally {
				StepManagerRegistry.getInstance().unregisterManager(PlainK3ExecutionEngine.this);
			}
		}
	}


	@Override
	protected void executeEntryPoint() {
		StepManagerRegistry.getInstance().registerManager(PlainK3ExecutionEngine.this);
		try {
			simulate();
		} catch (EngineStoppedException stopException) {
			// not really an error, simply forward the stop exception
			throw stopException;
		} catch (java.lang.reflect.InvocationTargetException ite) {
			// not really an error, simply forward the stop exception
			if (ite.getCause() instanceof EngineStoppedException) {
				throw (EngineStoppedException) ite.getCause();
			} else {
				throw new RuntimeException(ite);
			}
		} catch (Exception e) {
			throw new RuntimeException(e);
		} finally {
			StepManagerRegistry.getInstance().unregisterManager(PlainK3ExecutionEngine.this);
			if (server != null) {
				try {
					server.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	@Override
	/*
	 * This is the operation called from K3 code. We use this callback to pass the
	 * command to the generic executeOperation operation. (non-Javadoc)
	 * 
	 * @see fr.inria.diverse.k3.al.annotationprocessor.stepmanager.IStepManager#
	 * executeStep(java.lang.Object,
	 * fr.inria.diverse.k3.al.annotationprocessor.stepmanager.StepCommand,
	 * java.lang.String)
	 */
	public void executeStep(final Object caller, final StepCommand command, String className, final String methodName) {
		executeOperation(caller, className, methodName, new Runnable() {
			
			@Override
			public void run() {
				command.execute();
			}
		});
	}

	@Override
	/*
	 * This is the operation used to act as a StepManager in K3. We return true if
	 * we have the same editing domain as the object. (non-Javadoc)
	 * 
	 * @see fr.inria.diverse.k3.al.annotationprocessor.stepmanager.IStepManager#
	 * canHandle (java.lang.Object)
	 */
	public boolean canHandle(Object caller) {
		if (caller instanceof EObject) {
			EObject eObj = (EObject) caller;
			org.eclipse.emf.transaction.TransactionalEditingDomain editingDomain = getEditingDomain(eObj);
			return editingDomain == this.editingDomain;

		}
		return false;
	}

	/**
	 * Return a bundle containing 'aspectClassName'.
	 * 
	 * Return null if not found.
	 */
	private Bundle findBundle(final GenericModelExecutionContext<ISequentialRunConfiguration> executionContext, String aspectClassName) {

		// Look using JavaWorkspaceScope as this is safer and will look in
		// dependencies
		IType mainIType = getITypeMainByWorkspaceScope(aspectClassName);

		Bundle bundle = null;
		String bundleName = null;
		if (mainIType != null) {
			IPackageFragmentRoot packageFragmentRoot = (IPackageFragmentRoot) mainIType.getPackageFragment()
					.getParent();

			bundleName = packageFragmentRoot.getPath().removeLastSegments(1).lastSegment().toString();
			if (bundleName != null) {

				// We try to look into an already loaded bundle
				bundle = Platform.getBundle(bundleName);
			}
		} else {
			// the main isn't visible directly from the workspace, try another
			// method
			bundle = _executionContext.getDslBundle();
		}

		return bundle;
	}

	/**
	 * search the bundle that contains the Main class. The search is done in the
	 * workspace scope (ie. if it is defined in the current workspace it will find
	 * it
	 * 
	 * @return the name of the bundle containing the Main class or null if not found
	 */
	private IType getITypeMainByWorkspaceScope(String className) {
		SearchPattern pattern = SearchPattern.createPattern(className, IJavaSearchConstants.CLASS,
				IJavaSearchConstants.DECLARATIONS, SearchPattern.R_EXACT_MATCH);
		IJavaSearchScope scope = SearchEngine.createWorkspaceScope();

		final List<IType> binaryType = new ArrayList<IType>();

		SearchRequestor requestor = new SearchRequestor() {
			@Override
			public void acceptSearchMatch(SearchMatch match) throws CoreException {
				binaryType.add((IType) match.getElement());
			}
		};
		SearchEngine engine = new SearchEngine();

		try {
			engine.search(pattern, new SearchParticipant[] { SearchEngine.getDefaultSearchParticipant() }, scope,
					requestor, null);
		} catch (CoreException e1) {
			throw new RuntimeException("Error while searching the bundle: " + e1.getMessage());
			// return new Status(IStatus.ERROR, Activator.PLUGIN_ID, );
		}

		return binaryType.isEmpty() ? null : binaryType.get(0);
	}

	private static TransactionalEditingDomain getEditingDomain(EObject o) {
		return getEditingDomain(o.eResource().getResourceSet());
	}

	private static InternalTransactionalEditingDomain getEditingDomain(ResourceSet rs) {
		TransactionalEditingDomain edomain = org.eclipse.emf.transaction.TransactionalEditingDomain.Factory.INSTANCE
				.getEditingDomain(rs);
		if (edomain instanceof InternalTransactionalEditingDomain)
			return (InternalTransactionalEditingDomain) edomain;
		else
			return null;
	}

	/**
	 * Load the model for the given URI
	 * 
	 * @param modelURI
	 *            to load
	 * @return the loaded resource
	 */
	public static Resource loadModel(URI modelURI) {
		Resource resource = null;
		ResourceSet resourceSet;
		resourceSet = new ResourceSetImpl();
		resource = resourceSet.createResource(modelURI);
		try {
			resource.load(null);
		} catch (IOException e) {
			// chut
		}
		return resource;
	}

	
	
	
	
	
	
	
	
	private void decrement(Semaphore sem) {
		try {
			sem.acquire();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	private void increment(Semaphore sem) {
			sem.release();
	}
	
	
	Semaphore startDoStepSemaphore  = new Semaphore(0);
	Semaphore finishDoStepSemaphore = new Semaphore(0);
	Semaphore simulationEnded       = new Semaphore(0);
	CoordinationPredicate currentPredicate;
	AtomicDouble lastknownTime = new AtomicDouble(-1.0);
	double timeBeforeDoStep = -1;
	StopCondition lastStopcondition = null;
	Map<String, Object> qualifiedNameToEObject = new HashMap<>(); //initialized in prepareInitializeModel, used in GetVariable
	Map<String, Method> qualifiedNameToMethod = new HashMap<>(); 
	Object timeObject = null;
	Class<?> timeType = null;
	ServerSocket server = null;
	
	protected void simulate() throws Exception {
		//used to retrieve exceptions from the solverThread
		BlockingQueue<Exception> exceptionQueue = new ArrayBlockingQueue<>(1);

		
		Thread solverThread = launchEntryPointInThread(exceptionQueue);
		solverThread.start();
		
		server = new ServerSocket(39635);
		Socket clientSocket = server.accept();
		
		ObjectInputStream cin = new ObjectInputStream(clientSocket.getInputStream());
		ObjectOutputStream cout = new ObjectOutputStream(clientSocket.getOutputStream());
		
		Object clientCommand = cin.readObject();
		while(!simulationEnded.tryAcquire() || clientCommand instanceof StopCommand) { 
			System.out.println("Command received: "+clientCommand);
			if(clientCommand instanceof DoStepCommand) {
				timeBeforeDoStep = lastknownTime.doubleValue();
				System.out.println("DoStep starts @"+timeBeforeDoStep);
				currentPredicate = ((DoStepCommand) clientCommand).predicate;
				StopCondition stopCond = this.doStep(currentPredicate);
				System.out.println("DoStep stops @"+stopCond.timeValue+" due to "+stopCond.stopReason);
				cout.writeObject(stopCond);
			}	
			if(clientCommand instanceof GetVariableCommand) {
				String varQN = ((GetVariableCommand) clientCommand).variableQualifiedName;
				Object varValue= this.getVariable(varQN);
				cout.writeObject(varValue);
			}
			if(clientCommand instanceof SetVariableCommand) {
				String varQN = ((SetVariableCommand) clientCommand).variableQualifiedName;
				Object newValue = ((SetVariableCommand) clientCommand).newValue; 
				Boolean res = this.setVariable(varQN, newValue);
				cout.writeObject(res);
			}
			System.out.println("wait for a new command.");
			clientCommand = cin.readObject();
		}
		
		solverThread.join();
		server.close();
		
		if (exceptionQueue.isEmpty()) {
			return;
		}else {
			throw exceptionQueue.remove();
		}
	}

	public Thread launchEntryPointInThread(BlockingQueue<Exception> exceptionQueue) {
		return new Thread( () -> 
			// since aspect's methods are static, first arg is null
			{
				try {
					decrement(startDoStepSemaphore);
					this.entryPointMethod.invoke(null, entryPointMethodParameters.get(0));
				} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
					exceptionQueue.offer(e);
				}
				increment(simulationEnded);
			}
		);
	}
	
	public Object getVariable(String varQN) {
		Object var = qualifiedNameToEObject.get(varQN);
		Method method = qualifiedNameToMethod.get(varQN+"::get");
		if (var == null || method == null) {
			return false;
		}
		try {
			return method.invoke(var);
		} catch (SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			e.printStackTrace();
		}
		
		return true;
	}
	
	public boolean setVariable(String varQN, Object newValue) {
		Object var = qualifiedNameToEObject.get(varQN);
		Method method = qualifiedNameToMethod.get(varQN+"::set");
		if (var == null || method == null) {
			return false;
		}
		try {
			method.invoke(var, newValue);
		} catch (SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			e.printStackTrace();
		}
		
		return true;
	}

	
	public StopCondition doStep(CoordinationPredicate predicate) {
		increment(startDoStepSemaphore);
		decrement(finishDoStepSemaphore);
		return lastStopcondition;
		//should do something Here according to what has been done during the step (according to the stopCondition)
	}
	
	@Override
	public void readyToRead(EObject caller, Object propContainer, StepCommand command, String className, String propertyName) {
		System.out.println("in readyToRead("+propertyName+") on "+EcoreQNHelper.getQualifiedName(caller));
		if(currentPredicate != null && currentPredicate.contains(StopReason.READYTOREAD ,caller,className,propertyName)) {
			//struct to communicate the stop condition to the server
			lastStopcondition = new StopCondition(StopReason.READYTOREAD, /*caller, propContainer, command,*/ EcoreQNHelper.getQualifiedName(caller), propertyName, lastknownTime.floatValue()); //always lastKnownTime ?
			increment(finishDoStepSemaphore); // give control back to the caller.
			//wait for stuff to be done in the server before to continue
			decrement(startDoStepSemaphore); //waiting for the caller to do something
		}
		command.execute();
	}

	

	@Override
	public void justUpdated(EObject caller, Object propContainer, StepCommand command, String className, String propertyName) {
		System.out.println("in justUpdated("+propertyName+") on "+EcoreQNHelper.getQualifiedName(caller));
		command.execute();
		if(currentPredicate != null && currentPredicate.contains(StopReason.UPDATE ,caller,className,propertyName)) {
			//struct to communicate the stop condition to the server
			lastStopcondition = new StopCondition(StopReason.UPDATE, /*caller, propContainer, command,*/ EcoreQNHelper.getQualifiedName(caller), propertyName, lastknownTime.floatValue()); //always lastKnownTime ?
			increment(finishDoStepSemaphore); // give control back to the caller.
			//Optionally waiting for the caller to do something before to continue (logically we wait for antoher doStep)
			decrement(startDoStepSemaphore); 
		}
	}

	@Override
	public void timePassed(EObject caller, Object propContainer, StepCommand command, String className, double value) {
		System.out.println("in timePassed("+value+") on "+EcoreQNHelper.getQualifiedName(caller));
		command.execute();
		lastknownTime.set(value);
		
		if (currentPredicate != null && currentPredicate.contains(StopReason.TIME ,caller,className, Double.toString(value))) {
			double delta = ((TemporalPredicate)currentPredicate.getTemporalPredicate()).deltaT;
			if ((value - timeBeforeDoStep) >= delta){
				//struct to communicate the stop condition to the server
				lastStopcondition = new StopCondition(StopReason.TIME, /*caller, propContainer, command, */EcoreQNHelper.getQualifiedName(caller), "time", lastknownTime.floatValue()); //always lastKnownTime ?
				increment(finishDoStepSemaphore); // give control back to the caller.
				//wait for stuff to be done in the server before to continue
				decrement(startDoStepSemaphore); //waiting for the caller to do something
			}
		}
	}
	
	
	
	
	

}
