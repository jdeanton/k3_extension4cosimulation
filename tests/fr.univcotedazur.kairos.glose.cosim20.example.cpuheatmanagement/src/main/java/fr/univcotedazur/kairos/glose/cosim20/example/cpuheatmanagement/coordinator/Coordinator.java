package fr.univcotedazur.kairos.glose.cosim20.example.cpuheatmanagement.coordinator;

import org.eclipse.gemoc.execution.commons.commands.StopCondition;
import org.eclipse.gemoc.execution.commons.commands.StopReason;
import org.eclipse.gemoc.execution.commons.predicates.BinaryPredicate;
import org.eclipse.gemoc.execution.commons.predicates.EventPredicate;
import org.eclipse.gemoc.execution.commons.predicates.ReadyToReadPredicate;
import org.javafmi.modeldescription.ScalarVariable;
import org.javafmi.wrapper.Simulation;

public class Coordinator {

    public Simulation boxSU = null;
    public Simulation fanController = null;
    public OverHeatControllerSimulationUnit controlerSU = null;
    public double now = 0;


    public Coordinator(){
        boxSU = new Simulation("/home/jdeanton/boulot/recherche/articles/2020/cosim-CPS/workspace/fr.univcotedazur.kairos.glose.cosim20.example.cpuheatmanagement/src/fmu/CPUinBoxWithFanHeatModel.fmu");
        fanController = new Simulation("/home/jdeanton/boulot/recherche/articles/2020/cosim-CPS/workspace/fr.univcotedazur.kairos.glose.cosim20.example.cpuheatmanagement/src/fmu/FanController.fmu");

        controlerSU = new OverHeatControllerSimulationUnit("localhost", 39635);
    }

    public void loadBoxSU(){
        // Retrieve the FMU
        boxSU.init(0);

        boxSU.write("isStopped").with(false);
        boxSU.write("CPUfanSpeed").with(1);
        // Print FMU info
        System.out.println(boxSU.getModelDescription().getModelName() +" FMU I/O");
        for (ScalarVariable p : boxSU.getModelDescription().getModelVariables()) {
            if(p.getCausality().compareTo("input") == 0 || p.getCausality().compareTo("output") == 0) {
                System.out.println("\t" + p.getCausality() + " " + p.getName() + ":" + p.getTypeName());
            }
        }
    }
    public void loadFanControllerSU(){
        // Retrieve the FMU
        fanController.init(0);

        fanController.write("targetTemperature").with(65);
        fanController.write("Kp").with(5.0);
        // Print FMU info
        System.out.println(fanController.getModelDescription().getModelName() +" FMU I/O");
        for (ScalarVariable p : fanController.getModelDescription().getModelVariables()) {
            if(p.getCausality().compareTo("input") == 0 || p.getCausality().compareTo("output") == 0) {
                System.out.println("\t" + p.getCausality() + " " + p.getName() + ":" + p.getTypeName());
            }
        }
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        controlerSU.finalize();
        boxSU.terminate();
    }

    public static void main(String[] args) {
        Coordinator c = new Coordinator();
        c.loadBoxSU();
        c.loadFanControllerSU();
        c.controlerSU.load();
        boolean initialIsStopped = false;

        c.now = 0;
        System.out.println("let's start");
        while (c.now < 50000){
//            System.out.println("now = " +now);
            ReadyToReadPredicate r2rp = new ReadyToReadPredicate("currentValue", "CPUprotection::cpuTemperature");
            EventPredicate ep = new EventPredicate("occurs", "CPUprotection::switchCPUState");
            BinaryPredicate bp = new BinaryPredicate(r2rp, ep);
            StopCondition sc = c.controlerSU.doStep(bp);
//            System.out.println("\t" +sc.stopReason + " " + sc.propertyName + " @ " + sc.timeValue);
            if (sc.stopReason == StopReason.READYTOREAD) {
                simulateBoxAndFanControlUntilExpectedTime(c, sc.timeValue);
                double cpuTemperature = c.boxSU.read("CPUTemperature").asDouble();
//                System.out.println("\tcpuTemperature = " + cpuTemperature + "@" + sc.timeValue);
                double boxTemperature = c.boxSU.read("BoxTemperature").asDouble();
                System.out.println(c.now+","+cpuTemperature+","+boxTemperature+", ");
                c.controlerSU.setVariable("CPUprotection::cpuTemperature::currentValue", new Integer((int) cpuTemperature));
            } else { //event occured
                simulateBoxAndFanControlUntilExpectedTime(c, sc.timeValue);
                initialIsStopped = !initialIsStopped;
                c.boxSU.write("isStopped").with(initialIsStopped);
//                if (initialIsStopped){
//                    System.out.println("\tswitch CPU to off");
//                }else{
//                    System.out.println("\tswitch CPU to on");
//                }
            }
            //c.now = sc.timeValue;
        }

        c.controlerSU.terminate();
        c.boxSU.terminate();
        c.fanController.terminate();


    }

    private static void simulateBoxAndFanControlUntilExpectedTime(Coordinator c, double expectedTime) {
        double delta = expectedTime - c.now;
        while (delta + (c.now % 5) >= 5) { //\Delta t == 5 for each connector from boxSU to fanControllerSU
            double stepToDo = (5-(c.now % 5));
            c.boxSU.doStep(stepToDo);
            c.fanController.doStep(5);
            double cpuTemperature = c.boxSU.read("CPUTemperature").asDouble();
            c.fanController.write("CPUTemperature").with(cpuTemperature);
            int fanCommand = c.fanController.read("CPUfanSpeed").asInteger();
            c.boxSU.write("CPUfanSpeed").with(fanCommand);
            double boxTemperature = c.boxSU.read("BoxTemperature").asDouble();
            c.now += stepToDo;
            delta = expectedTime - c.now;
            System.out.println(c.now+","+cpuTemperature+","+boxTemperature+","+fanCommand);
        }
        if (delta > 0) {
            c.boxSU.doStep(delta);
            c.now += delta;
        }
    }

}
