//============================================================================
// Name        : main.cpp
// Author      : Julien Deantoni
// Version     : the best of ones I had :)
// Copyright   : http://i3s.unice.fr/~deantoni
// Description : Code generated for your CCSL specification, C++11 style
//============================================================================

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Semaphore;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PosixParser;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.gemoc.execution.commons.commands.DoStepCommand;
import org.eclipse.gemoc.execution.commons.commands.GetVariableCommand;
import org.eclipse.gemoc.execution.commons.commands.SetVariableCommand;
import org.eclipse.gemoc.execution.commons.commands.StopCommand;
import org.eclipse.gemoc.execution.commons.commands.StopCondition;
import org.eclipse.gemoc.execution.commons.commands.StopReason;
import org.eclipse.gemoc.execution.commons.helpers.EcoreQNHelper;
import org.eclipse.gemoc.execution.commons.predicates.CoordinationPredicate;
import org.eclipse.gemoc.execution.commons.predicates.EventPredicate;
import org.eclipse.gemoc.execution.commons.predicates.TemporalPredicate;
import org.eclipse.gemoc.executionframework.engine.commons.GenericModelExecutionContext;
import org.eclipse.gemoc.executionframework.engine.commons.sequential.ISequentialRunConfiguration;
import org.eclipse.gemoc.executionframework.engine.core.AbstractCommandBasedSequentialExecutionEngine;
import org.eclipse.gemoc.moccml.compiled.library.Clock;
import org.eclipse.gemoc.moccml.compiled.library.Solver;
import org.eclipse.gemoc.moccml.compiled.library.classicalexpressions.Not;
import org.eclipse.gemoc.moccml.compiled.library.classicalexpressions.SeqEmpty;
import org.eclipse.gemoc.moccml.compiled.library.classicalexpressions.SeqHead;
import org.eclipse.gemoc.moccml.compiled.library.classicalexpressions.SeqTail;
import org.eclipse.gemoc.moccml.compiled.library.constraints.Constraint;
import org.eclipse.gemoc.moccml.compiled.library.constraints.expressions.Concatenation;
import org.eclipse.gemoc.moccml.compiled.library.constraints.expressions.ConditionalExpression;
import org.eclipse.gemoc.moccml.compiled.library.constraints.expressions.ConditionalExpression.Case;
import org.eclipse.gemoc.moccml.compiled.library.constraints.expressions.Death;
import org.eclipse.gemoc.moccml.compiled.library.constraints.expressions.Defer;
import org.eclipse.gemoc.moccml.compiled.library.constraints.expressions.Union;
import org.eclipse.gemoc.moccml.compiled.library.constraints.expressions.UserDefinedExpression;
import org.eclipse.gemoc.moccml.compiled.library.constraints.expressions.Wait;
import org.eclipse.gemoc.moccml.compiled.library.constraints.relations.Coincides;
import org.eclipse.gemoc.moccml.compiled.library.constraints.relations.ConditionalRelation;
import org.eclipse.gemoc.moccml.compiled.library.constraints.relations.Exclusion;
import org.eclipse.gemoc.moccml.compiled.library.constraints.relations.Precedes;
import org.eclipse.gemoc.moccml.compiled.library.utils.QuantumBoolean;
import org.eclipse.gemoc.moccml.compiled.library.utils.Sequence;

import com.google.common.util.concurrent.AtomicDouble;

import fr.inria.diverse.k3.al.annotationprocessor.Aspect;
import fr.inria.diverse.k3.al.annotationprocessor.coordination.ICoordinationManager;
import fr.inria.diverse.k3.al.annotationprocessor.coordination.Input;
import fr.inria.diverse.k3.al.annotationprocessor.coordination.Output;
import fr.inria.diverse.k3.al.annotationprocessor.coordination.Time;
import fr.inria.diverse.k3.al.annotationprocessor.stepmanager.StepCommand;
import fr.inria.diverse.k3.al.annotationprocessor.stepmanager.StepManagerRegistry;
import toools.io.JavaResource;
import toools.io.file.RegularFile;

public class CompiledExecutionEngine  extends AbstractCommandBasedSequentialExecutionEngine<GenericModelExecutionContext<ISequentialRunConfiguration>, ISequentialRunConfiguration>
implements ICoordinationManager {
	
	public CompiledExecutionEngine() {
	}

	
	@Override
	public boolean canHandle(Object caller) {
		return true;
	}



	@Override
	public String engineKindName() {
		return null;
	}
	

	public Object execute(Object caller, String methodName, List<Object> parameters) {
		return internal_execute(caller, methodName, parameters, null);
	}

	private Object internal_execute(Object caller, String methodName, Collection<Object> parameters,
			Object mseOccurrence) throws RuntimeException {
		ArrayList<Object> staticParameters = new ArrayList<Object>();
		staticParameters.add(caller);
		if (parameters != null) {
			staticParameters.addAll(parameters);
		}
		Method bestApplicableMethod = getBestApplicableMethod(caller, methodName, staticParameters);
		if (bestApplicableMethod == null)
			throw new RuntimeException("static class not found or method not found when calling " + methodName
					+ " on " + caller + ". MSEOccurence=" + mseOccurrence);

		Object[] args = new Object[0];
		if (staticParameters != null) {
			args = staticParameters.toArray();
		}
		Object result = null;
		try {
			result = bestApplicableMethod.invoke(null, args);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			e.printStackTrace();
			throw new RuntimeException("Exception caught during execution of a call, see inner exception."+ methodName
					+ " on " + caller + ". MSEOccurence=" + mseOccurrence);
		}
		return result;
	}

	private Method getBestApplicableMethod(Object caller, String methodName, List<Object> parameters) {
		Set<Class<?>> staticHelperClasses = getStaticHelperClasses(caller);
		if (staticHelperClasses == null || staticHelperClasses.isEmpty()) {
			return null;
		}
		for (Class<?> c : staticHelperClasses) {
			Method m = getFirstApplicableMethod(c, methodName, parameters);
			if (m != null)
				return m;
		}
		return null;
	}

	/**
	 * return the first compatible method, goes up the inheritance hierarchy
	 * 
	 * @param staticHelperClass
	 * @param methodName
	 * @param parameters
	 * @return
	 */
	protected Method getFirstApplicableMethod(Class<?> staticHelperClass, String methodName, List<Object> parameters) {
		Method[] methods = staticHelperClass.getDeclaredMethods();
		for (Method method : methods) {
			Class<?>[] evaluatedMethodParamTypes = method.getParameterTypes();
			if (method.getName().equals(methodName) && evaluatedMethodParamTypes.length == parameters.size()) {
				boolean isAllParamCompatible = true;
				for (int i = 0; i < evaluatedMethodParamTypes.length; i++) {
					Object p = parameters.get(i);
					if (evaluatedMethodParamTypes[i].isPrimitive()) {

						if (evaluatedMethodParamTypes[i].equals(Integer.TYPE) && !Integer.class.isInstance(p)) {
							isAllParamCompatible = false;
							break;
						} else if (evaluatedMethodParamTypes[i].equals(Boolean.TYPE) && !Boolean.class.isInstance(p)) {
							isAllParamCompatible = false;
							break;
						}

					} else if (!evaluatedMethodParamTypes[i].isInstance(p)) {
						isAllParamCompatible = false;
						break;
					}
				}
				if (isAllParamCompatible) {
					return method;
				}
			}
		}
		// tries going in the inheritance hierarchy
		Class<?> superClass = staticHelperClass.getSuperclass();
		if (superClass != null)
			return getFirstApplicableMethod(superClass, methodName, parameters);
		else
			return null;
	}

	/**
	 * search static class by name (future version should use a map of available
	 * aspects, and deals with it as a list of applicable static classes)
	 * 
	 */
	protected Set<Class<?>> getStaticHelperClasses(Object target) {
		List<Class<?>> res = new ArrayList<>();
		for(Class<?> klass : savedAspects) {
			Aspect annot = (Aspect) klass.getAnnotation(Aspect.class);
			if (annot != null){
				if ((annot.className().isInstance(target))) {
					res.add(klass);
				}
			}
		}
		return new HashSet<Class<?>>(res);
	}


private void initializeCSV(ArrayList<ArrayList<String>> rtdCouples,
			EObject root) {
		try {
			csvFile.setContentAsASCII("");
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		for(int i = 0; i < rtdCouples.size(); i++) {
			ArrayList<String> couple = rtdCouples.get(i);
			TreeIterator<EObject> it = root.eAllContents();
			while (it.hasNext()) {
				EObject eo = it.next();
				String className = couple.get(0);
				try {
					if(Class.forName(className).isAssignableFrom(eo.getClass())) {
						if (i == 0) {
							csvFile.append("timeRef".getBytes());
						}else {
							csvFile.append((","+getEObjectName(eo)).getBytes());
						}
					}
				} catch (ClassNotFoundException | IOException e) {
					e.printStackTrace();
				}
			}
		}
		try {
			csvFile.append(("\n").getBytes());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
 	private void generatePlotScript(ArrayList<ArrayList<String>> rtdCouples,
			EObject root) {
		RegularFile plotFile = new RegularFile("org.eclipse.gemoc.example.moccmlsigpml.simple_example.executable.gnuplot");
		try {
			plotFile.setContentAsASCII("");
			plotFile.append((" set title 'result1'\n" + 
				" set ylabel 'RTD'\n" + 
				" set xlabel 'Time'\n" + 
				" set grid\n" + 
				" set term pdf\n" + 
				" set output 'org.eclipse.gemoc.example.moccmlsigpml.simple_example.executable.pdf'\n" + 
				" plot ").getBytes());
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		String sep="";
		int column = 2;
		for(int i = 0; i < rtdCouples.size(); i++) {
			ArrayList<String> couple = rtdCouples.get(i);
			TreeIterator<EObject> it = root.eAllContents();
			while (it.hasNext()) {
				EObject eo = it.next();
				String className = couple.get(0);
				try {
					if(Class.forName(className).isAssignableFrom(eo.getClass())) {
						if (i == 0) {
							//nothing to do here
						}else {
							plotFile.append((sep+"'org.eclipse.gemoc.example.moccmlsigpml.simple_example.executable.csv' using 1:"+column+" with lines title '"+getEObjectName(eo)+"'").getBytes());
							sep=",";
							column++;
						}
					}
				} catch (ClassNotFoundException | IOException e) {
					e.printStackTrace();
				}
			}
		}
		try {
			plotFile.append(("\n").getBytes());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}	private String getEObjectName(EObject eo) {
		EStructuralFeature nameF = eo.eClass().getEStructuralFeature("name");
		String name = nameF != null ? (String)eo.eGet(nameF) : null;
		if (name != null) {
			return name;
		}else {
			return "noName";
		}
	}
	
	int timeRef = 0;
	RegularFile csvFile = new RegularFile("org.eclipse.gemoc.example.moccmlsigpml.simple_example.executable.csv");
		private void logRTDs(ArrayList<ArrayList<String>> rtdCouples,
			EObject root) {
		
		ArrayList<Object> results = new ArrayList<Object>();
		Object aResult = -1;
			for(int i = 0; i < rtdCouples.size(); i++) {
				ArrayList<String> couple = rtdCouples.get(i);
				TreeIterator<EObject> it = root.eAllContents();
				while (it.hasNext()) {
					EObject eo = it.next();
					String className = couple.get(0);
					String rtdName = couple.get(1);
					try {
						if(Class.forName(className).isAssignableFrom(eo.getClass())) {
							try{
								aResult = (Integer) this.execute(eo, rtdName, null);
							}
							catch(ClassCastException c) {
								aResult = (Float) this.execute(eo, rtdName, null);
							}
							if (i == 0) {
								timeRef = (int) aResult;
							}else {
								results.add(aResult);
							}
						}
					} catch (ClassNotFoundException e) {
						e.printStackTrace();
					}
				}
			}
			
			try {
				csvFile.append((""+timeRef).getBytes());
				for(Object i : results) {
					csvFile.append((", "+i.toString()).getBytes());
				}
				csvFile.append(("\n").getBytes());
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		}



	Set<Class<?>> savedAspects = new HashSet<>(Arrays.asList(           org.eclipse.gemoc.example.moccmlsigpml.k3dsa.SystemAspect.class
           ,org.eclipse.gemoc.example.moccmlsigpml.k3dsa.AgentAspect.class
           ,org.eclipse.gemoc.example.moccmlsigpml.k3dsa.OutputPortAspect.class
           ,org.eclipse.gemoc.example.moccmlsigpml.k3dsa.NamedElementAspect.class
           ,org.eclipse.gemoc.example.moccmlsigpml.k3dsa.InputPortAspect.class
           ,org.eclipse.gemoc.example.moccmlsigpml.k3dsa.PlaceAspect.class
           ,org.eclipse.gemoc.example.moccmlsigpml.k3dsa.HWComputationalResourceAspect.class
			));

	
	
    Map<Clock, String> clockToMethodName= new HashMap<>();
    Map<Clock, String> clockToEObject = new HashMap<>();
	String languageName="xSigPML";
	public static void main(String[] args) {
           int nbSteps = 500; //by default
		boolean logClocks = false;
		boolean logStepsOnly = false;
		
		Options options = new Options();

		Option opt_nbSteps = new Option("n", "nbSteps", true, "number of steps to execute (-1 for infinite)");
		opt_nbSteps.setRequired(false);
        options.addOption(opt_nbSteps);
		
        Option opt_logClocks = new Option("l", "logClocksAndSteps", false, "log clock status and execution steps");
        opt_logClocks.setRequired(false);
        options.addOption(opt_logClocks);
        
        Option opt_logStepsOnly = new Option("s", "logStepsOnly", false, "log only execution steps");
        opt_logStepsOnly.setRequired(false);
        options.addOption(opt_logStepsOnly);
        
        Option opt_rtdToLog = new Option("rtd", "runtimeDataToLog", false, "list of couple with class name and rtd name");
        opt_rtdToLog.setRequired(false);
        opt_rtdToLog.setArgs(Option.UNLIMITED_VALUES);
        opt_rtdToLog.setValueSeparator(' ');
        options.addOption(opt_rtdToLog);
        
        
        

        CommandLineParser parser = new PosixParser();
        HelpFormatter formatter = new HelpFormatter();
        CommandLine cmd = null;

        try {
            cmd = parser.parse(options, args);
        } catch (ParseException e) {
            System.out.println(e.getMessage());
            formatter.printHelp("Compiled Execution Engine", options);

            System.exit(1);
        }

        if (cmd.hasOption("nbSteps")) nbSteps= Integer.parseInt(cmd.getOptionValue("nbSteps"));
        logClocks = cmd.hasOption("logClocksAndSteps");
        logStepsOnly = cmd.hasOption("logStepsOnly");
        String[] rtdList = null;
		if (cmd.hasOption("rtd")) rtdList = cmd.getOptionValues("rtd");
	    if (rtdList != null && rtdList.length%2 != 0) {
	    	System.err.println("RTD not considered since not correctly formatted. (list of couples, a couples being a class (qualified) name and a rtd name");
	    }
		ArrayList<ArrayList<String>> rtdCouples = new ArrayList<ArrayList<String>>();
		if (rtdList != null){
         for(int i = 0; i < rtdList.length; i+=2) {
			    rtdCouples.add(new ArrayList<String>(Arrays.asList(rtdList[i], rtdList[i+1])));
           }
		}

		CompiledExecutionEngine theEngine = new CompiledExecutionEngine();
		
	 int simpleSystem_0_mainBlock_pA1inState_3pA1inState_3_rate = 1;
	 int simpleSystem_0_mainBlock_pA1inState_3_ratePlusOne = 2;
	 Integer[] finite_simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_ByWeightPlusOne = {};
	 Integer[] infinite_simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_ByWeightPlusOne = {4};
	 Sequence simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_ByWeightPlusOne = new Sequence(finite_simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_ByWeightPlusOne,infinite_simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_ByWeightPlusOne,"simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_ByWeightPlusOne");
	 Integer[] finite_simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_ByWeightPlusOne = {};
	 Integer[] infinite_simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_ByWeightPlusOne = {5};
	 Sequence simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_ByWeightPlusOne = new Sequence(finite_simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_ByWeightPlusOne,infinite_simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_ByWeightPlusOne,"simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_ByWeightPlusOne");
	 int simpleSystem_0_mainBlock_pA2in_4pA2in_4_rate = 4;
	 int simpleSystem_0_mainBlock_placeA1State_8_sizePlusOne = 2;
	 Integer[] finite_simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_ByWeight = {};
	 Integer[] infinite_simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_ByWeight = {4};
	 Sequence simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_ByWeight = new Sequence(finite_simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_ByWeight,infinite_simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_ByWeight,"simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_ByWeight");
	 Integer[] finite_simpleSystem_0_mainBlock_CPU1_9Alternates_NonPreemptiveExecution_AlternatesDef_seqOneInfinite = {};
	 Integer[] infinite_simpleSystem_0_mainBlock_CPU1_9Alternates_NonPreemptiveExecution_AlternatesDef_seqOneInfinite = {1};
	 Sequence simpleSystem_0_mainBlock_CPU1_9Alternates_NonPreemptiveExecution_AlternatesDef_seqOneInfinite = new Sequence(finite_simpleSystem_0_mainBlock_CPU1_9Alternates_NonPreemptiveExecution_AlternatesDef_seqOneInfinite,infinite_simpleSystem_0_mainBlock_CPU1_9Alternates_NonPreemptiveExecution_AlternatesDef_seqOneInfinite,"simpleSystem_0_mainBlock_CPU1_9Alternates_NonPreemptiveExecution_AlternatesDef_seqOneInfinite");
	 int simpleSystem_0_mainBlock_place1_7Token_TokenConstraint_TokenDef_zero = 0;
	 int simpleSystem_0_mainBlock_placeA1State_8pA1inState_3_rate = 1;
	 boolean simpleSystem_0_mainBlock_place1_7Token_TokenConstraint_TokenDef_11 = false;
	 boolean simpleSystem_0_mainBlock_placeA1State_8Token_TokenConstraint_TokenDef_12 = false;
	 Integer[] finite_simpleSystem_0_mainBlock_A2_1Alternates_NonReentrantAgent_AlternatesDef_seqOneInfinite = {};
	 Integer[] infinite_simpleSystem_0_mainBlock_A2_1Alternates_NonReentrantAgent_AlternatesDef_seqOneInfinite = {1};
	 Sequence simpleSystem_0_mainBlock_A2_1Alternates_NonReentrantAgent_AlternatesDef_seqOneInfinite = new Sequence(finite_simpleSystem_0_mainBlock_A2_1Alternates_NonReentrantAgent_AlternatesDef_seqOneInfinite,infinite_simpleSystem_0_mainBlock_A2_1Alternates_NonReentrantAgent_AlternatesDef_seqOneInfinite,"simpleSystem_0_mainBlock_A2_1Alternates_NonReentrantAgent_AlternatesDef_seqOneInfinite");
	 int simpleSystem_0_mainBlock_place1_7_delayPlusOne = 1;
	 Integer[] finite_simpleSystem_0_mainBlock_A1_2Alternates_NonReentrantAgent_AlternatesDef_seqOneInfinite = {};
	 Integer[] infinite_simpleSystem_0_mainBlock_A1_2Alternates_NonReentrantAgent_AlternatesDef_seqOneInfinite = {1};
	 Sequence simpleSystem_0_mainBlock_A1_2Alternates_NonReentrantAgent_AlternatesDef_seqOneInfinite = new Sequence(finite_simpleSystem_0_mainBlock_A1_2Alternates_NonReentrantAgent_AlternatesDef_seqOneInfinite,infinite_simpleSystem_0_mainBlock_A1_2Alternates_NonReentrantAgent_AlternatesDef_seqOneInfinite,"simpleSystem_0_mainBlock_A1_2Alternates_NonReentrantAgent_AlternatesDef_seqOneInfinite");
	 int simpleSystem_0_mainBlock_pA2in_4_ratePlusOne = 5;
	 Integer[] finite_simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_ByWeight = {};
	 Integer[] infinite_simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_ByWeight = {1};
	 Sequence simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_ByWeight = new Sequence(finite_simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_ByWeight,infinite_simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_ByWeight,"simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_ByWeight");
	 int simpleSystem_0_mainBlock_place1_7_sizePlusOne = 7;
	 int simpleSystem_0_mainBlock_pA1out_5pA1out_5_rate = 1;
	 int simpleSystem_0_mainBlock_pA1outState_6pA1outState_6_rate = 1;
	 Integer[] finite_simpleSystem_0_mainBlock_placeA1State_8_DelayFor_NwriteInAdvance_DelayForDef_seqForDelayFor = {};
	 Integer[] infinite_simpleSystem_0_mainBlock_placeA1State_8_DelayFor_NwriteInAdvance_DelayForDef_seqForDelayFor = {2};
	 Sequence simpleSystem_0_mainBlock_placeA1State_8_DelayFor_NwriteInAdvance_DelayForDef_seqForDelayFor = new Sequence(finite_simpleSystem_0_mainBlock_placeA1State_8_DelayFor_NwriteInAdvance_DelayForDef_seqForDelayFor,infinite_simpleSystem_0_mainBlock_placeA1State_8_DelayFor_NwriteInAdvance_DelayForDef_seqForDelayFor,"simpleSystem_0_mainBlock_placeA1State_8_DelayFor_NwriteInAdvance_DelayForDef_seqForDelayFor");
	 Integer[] finite_simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_oneThenByWeightPlusOne = {2};
	 Integer[] infinite_simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_oneThenByWeightPlusOne = {1};
	 Sequence simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_oneThenByWeightPlusOne = new Sequence(finite_simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_oneThenByWeightPlusOne,infinite_simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_oneThenByWeightPlusOne,"simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_oneThenByWeightPlusOne");
	 int simpleSystem_0_mainBlock_placeA1State_8Token_TokenConstraint_TokenDef_zero = 0;
	 Integer[] finite_simpleSystem_0_mainBlock_place1_7_DelayFor_NwriteInAdvance_DelayForDef_seqForDelayFor = {};
	 Integer[] infinite_simpleSystem_0_mainBlock_place1_7_DelayFor_NwriteInAdvance_DelayForDef_seqForDelayFor = {7};
	 Sequence simpleSystem_0_mainBlock_place1_7_DelayFor_NwriteInAdvance_DelayForDef_seqForDelayFor = new Sequence(finite_simpleSystem_0_mainBlock_place1_7_DelayFor_NwriteInAdvance_DelayForDef_seqForDelayFor,infinite_simpleSystem_0_mainBlock_place1_7_DelayFor_NwriteInAdvance_DelayForDef_seqForDelayFor,"simpleSystem_0_mainBlock_place1_7_DelayFor_NwriteInAdvance_DelayForDef_seqForDelayFor");
	 Integer[] finite_simpleSystem_0_mainBlock_place1_7Token_TokenConstraint_TokenDef_sDelay = {0};
	 Integer[] infinite_simpleSystem_0_mainBlock_place1_7Token_TokenConstraint_TokenDef_sDelay = {};
	 Sequence simpleSystem_0_mainBlock_place1_7Token_TokenConstraint_TokenDef_sDelay = new Sequence(finite_simpleSystem_0_mainBlock_place1_7Token_TokenConstraint_TokenDef_sDelay,infinite_simpleSystem_0_mainBlock_place1_7Token_TokenConstraint_TokenDef_sDelay,"simpleSystem_0_mainBlock_place1_7Token_TokenConstraint_TokenDef_sDelay");
	 Integer[] finite_simpleSystem_0_mainBlock_pA1out_5Output_AgentOutputConstraint_OutputDef_Output_ByWeight = {1};
	 Integer[] infinite_simpleSystem_0_mainBlock_pA1out_5Output_AgentOutputConstraint_OutputDef_Output_ByWeight = {1};
	 Sequence simpleSystem_0_mainBlock_pA1out_5Output_AgentOutputConstraint_OutputDef_Output_ByWeight = new Sequence(finite_simpleSystem_0_mainBlock_pA1out_5Output_AgentOutputConstraint_OutputDef_Output_ByWeight,infinite_simpleSystem_0_mainBlock_pA1out_5Output_AgentOutputConstraint_OutputDef_Output_ByWeight,"simpleSystem_0_mainBlock_pA1out_5Output_AgentOutputConstraint_OutputDef_Output_ByWeight");
	 boolean simpleSystem_0_mainBlock_placeA1State_8Token_TokenConstraint_TokenDef_27 = true;
	 Integer[] finite_simpleSystem_0_mainBlock_placeA1State_8Token_TokenConstraint_TokenDef_sDelay = {1};
	 Integer[] infinite_simpleSystem_0_mainBlock_placeA1State_8Token_TokenConstraint_TokenDef_sDelay = {};
	 Sequence simpleSystem_0_mainBlock_placeA1State_8Token_TokenConstraint_TokenDef_sDelay = new Sequence(finite_simpleSystem_0_mainBlock_placeA1State_8Token_TokenConstraint_TokenDef_sDelay,infinite_simpleSystem_0_mainBlock_placeA1State_8Token_TokenConstraint_TokenDef_sDelay,"simpleSystem_0_mainBlock_placeA1State_8Token_TokenConstraint_TokenDef_sDelay");
	 Integer[] finite_simpleSystem_0_mainBlock_pA1outState_6Output_AgentOutputConstraint_OutputDef_Output_ByWeight = {1};
	 Integer[] infinite_simpleSystem_0_mainBlock_pA1outState_6Output_AgentOutputConstraint_OutputDef_Output_ByWeight = {1};
	 Sequence simpleSystem_0_mainBlock_pA1outState_6Output_AgentOutputConstraint_OutputDef_Output_ByWeight = new Sequence(finite_simpleSystem_0_mainBlock_pA1outState_6Output_AgentOutputConstraint_OutputDef_Output_ByWeight,infinite_simpleSystem_0_mainBlock_pA1outState_6Output_AgentOutputConstraint_OutputDef_Output_ByWeight,"simpleSystem_0_mainBlock_pA1outState_6Output_AgentOutputConstraint_OutputDef_Output_ByWeight");
	 int simpleSystem_0_mainBlock_A1_2A1_2_cycles = 2;
	 Integer[] finite_simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_StartThenByExecTime = {1};
	 Integer[] infinite_simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_StartThenByExecTime = {4};
	 Sequence simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_StartThenByExecTime = new Sequence(finite_simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_StartThenByExecTime,infinite_simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_StartThenByExecTime,"simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_StartThenByExecTime");
	 int simpleSystem_0_mainBlock_place1_7pA2in_4_rate = 4;
	 boolean simpleSystem_0_mainBlock_place1_7Token_TokenConstraint_TokenDef_33 = true;
	 Integer[] finite_simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_oneThenByWeightPlusOne = {1};
	 Integer[] infinite_simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_oneThenByWeightPlusOne = {4};
	 Sequence simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_oneThenByWeightPlusOne = new Sequence(finite_simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_oneThenByWeightPlusOne,infinite_simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_oneThenByWeightPlusOne,"simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_oneThenByWeightPlusOne");
	 Integer[] finite_simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_ByExecTime = {};
	 Integer[] infinite_simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_ByExecTime = {2};
	 Sequence simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_ByExecTime = new Sequence(finite_simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_ByExecTime,infinite_simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_ByExecTime,"simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_ByExecTime");
	 int simpleSystem_0_mainBlock_placeA1State_8_delayPlusOne = 2;
	 Integer[] finite_simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_ByWeightPlusOne = {};
	 Integer[] infinite_simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_ByWeightPlusOne = {2};
	 Sequence simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_ByWeightPlusOne = new Sequence(finite_simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_ByWeightPlusOne,infinite_simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_ByWeightPlusOne,"simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_ByWeightPlusOne");
	 Integer[] finite_simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_StartThenByExecTime = {1};
	 Integer[] infinite_simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_StartThenByExecTime = {2};
	 Sequence simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_StartThenByExecTime = new Sequence(finite_simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_StartThenByExecTime,infinite_simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_StartThenByExecTime,"simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_StartThenByExecTime");
	 int simpleSystem_0_mainBlock_place1_7place1_7_delay = 0;
	 int simpleSystem_0_mainBlock_A2_1A2_1_cycles = 4;
	 Integer[] finite_simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_ByWeightPlusOne = {};
	 Integer[] infinite_simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_ByWeightPlusOne = {1};
	 Sequence simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_ByWeightPlusOne = new Sequence(finite_simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_ByWeightPlusOne,infinite_simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_ByWeightPlusOne,"simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_ByWeightPlusOne");
	 int simpleSystem_0_mainBlock_placeA1State_8placeA1State_8_delay = 1;
	 Integer[] finite_simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_ByExecTime = {};
	 Integer[] infinite_simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_ByExecTime = {4};
	 Sequence simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_ByExecTime = new Sequence(finite_simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_ByExecTime,infinite_simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_ByExecTime,"simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_ByExecTime");
SeqHead simpleSystem_0_mainBlock_pA1outState_6Output_AgentOutputConstraint_OutputDef_Output_writeByWeight_FilterByDef_SeqHead = new SeqHead(simpleSystem_0_mainBlock_pA1outState_6Output_AgentOutputConstraint_OutputDef_Output_ByWeight);
Not simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_readByWeight_FilterByDef_45 = new Not( new SeqEmpty(simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_ByWeight));
SeqTail simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredStartThenByExecTime_FilterByDef_SeqTail = new SeqTail(simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_StartThenByExecTime);
SeqHead simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_readByWeightPlusOne_FilterByDef_SeqHead = new SeqHead(simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_ByWeightPlusOne);
SeqHead simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredStartThenByExecTime_FilterByDef_SeqHead = new SeqHead(simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_StartThenByExecTime);
SeqEmpty simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_readOneThenByWeightPlusOne_FilterByDef_49 = new SeqEmpty(simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_oneThenByWeightPlusOne);
SeqEmpty simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_readByWeightPlusOne_FilterByDef_50 = new SeqEmpty(simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_ByWeightPlusOne);
SeqEmpty simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_readByWeight_FilterByDef_51 = new SeqEmpty(simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_ByWeight);
SeqEmpty simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredByExecTime_FilterByDef_52 = new SeqEmpty(simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_ByExecTime);
SeqHead simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredByExecTime_FilterByDef_SeqHead = new SeqHead(simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_ByExecTime);
SeqHead simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_pushByWeightPlusOne_FilterByDef_SeqHead = new SeqHead(simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_ByWeightPlusOne);
SeqHead simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredStartThenByExecTime_FilterByDef_SeqHead = new SeqHead(simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_StartThenByExecTime);
Not simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_readOneThenByWeightPlusOne_FilterByDef_56 = new Not( new SeqEmpty(simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_oneThenByWeightPlusOne));
SeqHead simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_readByWeight_FilterByDef_SeqHead = new SeqHead(simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_ByWeight);
SeqTail simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_readByWeightPlusOne_FilterByDef_SeqTail = new SeqTail(simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_ByWeightPlusOne);
SeqHead simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_readByWeight_FilterByDef_SeqHead = new SeqHead(simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_ByWeight);
SeqHead simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredByExecTime_FilterByDef_SeqHead = new SeqHead(simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_ByExecTime);
Not simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredStartThenByExecTime_FilterByDef_61 = new Not( new SeqEmpty(simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_StartThenByExecTime));
SeqTail simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_readByWeightPlusOne_FilterByDef_SeqTail = new SeqTail(simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_ByWeightPlusOne);
SeqEmpty simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredStartThenByExecTime_FilterByDef_63 = new SeqEmpty(simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_StartThenByExecTime);
SeqTail simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_readByWeight_FilterByDef_SeqTail = new SeqTail(simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_ByWeight);
SeqEmpty simpleSystem_0_mainBlock_pA1out_5Output_AgentOutputConstraint_OutputDef_Output_writeByWeight_FilterByDef_65 = new SeqEmpty(simpleSystem_0_mainBlock_pA1out_5Output_AgentOutputConstraint_OutputDef_Output_ByWeight);
Not simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredStartThenByExecTime_FilterByDef_66 = new Not( new SeqEmpty(simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_StartThenByExecTime));
SeqTail simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredByExecTime_FilterByDef_SeqTail = new SeqTail(simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_ByExecTime);
Not simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_pushByWeightPlusOne_FilterByDef_68 = new Not( new SeqEmpty(simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_ByWeightPlusOne));
SeqEmpty simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_readOneThenByWeightPlusOne_FilterByDef_69 = new SeqEmpty(simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_oneThenByWeightPlusOne);
Not simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_readByWeightPlusOne_FilterByDef_70 = new Not( new SeqEmpty(simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_ByWeightPlusOne));
SeqEmpty simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_pushByWeightPlusOne_FilterByDef_71 = new SeqEmpty(simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_ByWeightPlusOne);
SeqTail simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_pushByWeightPlusOne_FilterByDef_SeqTail = new SeqTail(simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_ByWeightPlusOne);
SeqTail simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredByExecTime_FilterByDef_SeqTail = new SeqTail(simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_ByExecTime);
SeqHead simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_readOneThenByWeightPlusOne_FilterByDef_SeqHead = new SeqHead(simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_oneThenByWeightPlusOne);
SeqEmpty simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_pushByWeightPlusOne_FilterByDef_75 = new SeqEmpty(simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_ByWeightPlusOne);
Not simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_pushByWeightPlusOne_FilterByDef_76 = new Not( new SeqEmpty(simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_ByWeightPlusOne));
SeqTail simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_readOneThenByWeightPlusOne_FilterByDef_SeqTail = new SeqTail(simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_oneThenByWeightPlusOne);
Not simpleSystem_0_mainBlock_pA1outState_6Output_AgentOutputConstraint_OutputDef_Output_writeByWeight_FilterByDef_78 = new Not( new SeqEmpty(simpleSystem_0_mainBlock_pA1outState_6Output_AgentOutputConstraint_OutputDef_Output_ByWeight));
SeqHead simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_readByWeightPlusOne_FilterByDef_SeqHead = new SeqHead(simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_ByWeightPlusOne);
SeqEmpty simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredStartThenByExecTime_FilterByDef_80 = new SeqEmpty(simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_StartThenByExecTime);
SeqEmpty simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredByExecTime_FilterByDef_81 = new SeqEmpty(simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_ByExecTime);
SeqTail simpleSystem_0_mainBlock_pA1out_5Output_AgentOutputConstraint_OutputDef_Output_writeByWeight_FilterByDef_SeqTail = new SeqTail(simpleSystem_0_mainBlock_pA1out_5Output_AgentOutputConstraint_OutputDef_Output_ByWeight);
SeqEmpty simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_readByWeightPlusOne_FilterByDef_83 = new SeqEmpty(simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_ByWeightPlusOne);
Not simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredByExecTime_FilterByDef_84 = new Not( new SeqEmpty(simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_ByExecTime));
SeqTail simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_readOneThenByWeightPlusOne_FilterByDef_SeqTail = new SeqTail(simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_oneThenByWeightPlusOne);
SeqEmpty simpleSystem_0_mainBlock_pA1outState_6Output_AgentOutputConstraint_OutputDef_Output_writeByWeight_FilterByDef_86 = new SeqEmpty(simpleSystem_0_mainBlock_pA1outState_6Output_AgentOutputConstraint_OutputDef_Output_ByWeight);
SeqTail simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_pushByWeightPlusOne_FilterByDef_SeqTail = new SeqTail(simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_ByWeightPlusOne);
Not simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_readByWeight_FilterByDef_88 = new Not( new SeqEmpty(simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_ByWeight));
SeqHead simpleSystem_0_mainBlock_pA1out_5Output_AgentOutputConstraint_OutputDef_Output_writeByWeight_FilterByDef_SeqHead = new SeqHead(simpleSystem_0_mainBlock_pA1out_5Output_AgentOutputConstraint_OutputDef_Output_ByWeight);
Not simpleSystem_0_mainBlock_pA1out_5Output_AgentOutputConstraint_OutputDef_Output_writeByWeight_FilterByDef_90 = new Not( new SeqEmpty(simpleSystem_0_mainBlock_pA1out_5Output_AgentOutputConstraint_OutputDef_Output_ByWeight));
Not simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_readByWeightPlusOne_FilterByDef_91 = new Not( new SeqEmpty(simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_ByWeightPlusOne));
SeqHead simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_pushByWeightPlusOne_FilterByDef_SeqHead = new SeqHead(simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_ByWeightPlusOne);
SeqTail simpleSystem_0_mainBlock_pA1outState_6Output_AgentOutputConstraint_OutputDef_Output_writeByWeight_FilterByDef_SeqTail = new SeqTail(simpleSystem_0_mainBlock_pA1outState_6Output_AgentOutputConstraint_OutputDef_Output_ByWeight);
Not simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredByExecTime_FilterByDef_94 = new Not( new SeqEmpty(simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_ByExecTime));
SeqTail simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredStartThenByExecTime_FilterByDef_SeqTail = new SeqTail(simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_StartThenByExecTime);
SeqTail simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_readByWeight_FilterByDef_SeqTail = new SeqTail(simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_ByWeight);
SeqHead simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_readOneThenByWeightPlusOne_FilterByDef_SeqHead = new SeqHead(simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_oneThenByWeightPlusOne);
Not simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_readOneThenByWeightPlusOne_FilterByDef_98 = new Not( new SeqEmpty(simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_oneThenByWeightPlusOne));
SeqEmpty simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_readByWeight_FilterByDef_99 = new SeqEmpty(simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_ByWeight);
	 Clock simpleSystem_0_mainBlock_CPU1_9_isExecuting = new Clock("simpleSystem_0_mainBlock_CPU1_9_isExecuting");
	 Clock simpleSystem_0_mainBlock_pA1inState_3_read = new Clock("simpleSystem_0_mainBlock_pA1inState_3_read");
	 Clock simpleSystem_0_mainBlock_A2_1_startAgent = new Clock("simpleSystem_0_mainBlock_A2_1_startAgent");
	 Clock simpleSystem_0_mainBlock_mem1_10_isWriting = new Clock("simpleSystem_0_mainBlock_mem1_10_isWriting");
	 Clock simpleSystem_0_mainBlock_placeA1State_8_pop = new Clock("simpleSystem_0_mainBlock_placeA1State_8_pop");
	 Clock simpleSystem_0_mainBlock_A1_2_stopAgent = new Clock("simpleSystem_0_mainBlock_A1_2_stopAgent");
	 Clock simpleSystem_0_mainBlock_place1_7_pop = new Clock("simpleSystem_0_mainBlock_place1_7_pop");
	 Clock simpleSystem_0_mainBlock_pA1outState_6_write = new Clock("simpleSystem_0_mainBlock_pA1outState_6_write");
	 Clock simpleSystem_0_mainBlock_placeA1State_8_push = new Clock("simpleSystem_0_mainBlock_placeA1State_8_push");
	 Clock simpleSystem_0_mainBlock_CPU1_9_idle = new Clock("simpleSystem_0_mainBlock_CPU1_9_idle");
	 Clock simpleSystem_0_mainBlock_mem1_10_isReading = new Clock("simpleSystem_0_mainBlock_mem1_10_isReading");
	 Clock simpleSystem_0_mainBlock_A1_2_isExecuting = new Clock("simpleSystem_0_mainBlock_A1_2_isExecuting");
	 Clock simpleSystem_0_mainBlock_A2_1_stopAgent = new Clock("simpleSystem_0_mainBlock_A2_1_stopAgent");
	 Clock simpleSystem_0_mainBlock_A2_1_isExecuting = new Clock("simpleSystem_0_mainBlock_A2_1_isExecuting");
	 Clock simpleSystem_0_mainBlock_place1_7_push = new Clock("simpleSystem_0_mainBlock_place1_7_push");
	 Clock simpleSystem_0_mainBlock_A1_2_startAgent = new Clock("simpleSystem_0_mainBlock_A1_2_startAgent");
	 Clock simpleSystem_0_mainBlock_pA2in_4_read = new Clock("simpleSystem_0_mainBlock_pA2in_4_read");
	 Clock simpleSystem_0_mainBlock_pA1out_5_write = new Clock("simpleSystem_0_mainBlock_pA1out_5_write");
	Death simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_pushByWeightPlusOne_FilterByDef_killIt = new Death("simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_pushByWeightPlusOne_FilterByDef_killIt");
	Concatenation simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_readByWeight_FilterByDef_FilterBy_concatWait = new Concatenation("simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_readByWeight_FilterByDef_FilterBy_concatWait");
	Death simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_readOneThenByWeightPlusOne_FilterByDef_killIt = new Death("simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_readOneThenByWeightPlusOne_FilterByDef_killIt");
	Death simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_readByWeight_FilterByDef_killIt = new Death("simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_readByWeight_FilterByDef_killIt");
	Concatenation simpleSystem_0_mainBlock_pA1outState_6Output_AgentOutputConstraint_OutputDef_Output_writeByWeight_FilterByDef_FilterBy_concatWait = new Concatenation("simpleSystem_0_mainBlock_pA1outState_6Output_AgentOutputConstraint_OutputDef_Output_writeByWeight_FilterByDef_FilterBy_concatWait");
	Wait simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredByExecTime_FilterByDef_FilterBy_waitHead = new Wait(simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredByExecTime_FilterByDef_SeqHead,"simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredByExecTime_FilterByDef_FilterBy_waitHead");
	Defer simpleSystem_0_mainBlock_placeA1State_8_DelayFor_NwriteInAdvance_DelayForDef_DelayFor_defer = new Defer("simpleSystem_0_mainBlock_placeA1State_8_DelayFor_NwriteInAdvance_DelayForDef_DelayFor_defer");
	Concatenation simpleSystem_0_mainBlock_pA1out_5Output_AgentOutputConstraint_OutputDef_Output_writeByWeight_FilterByDef_FilterBy_concatWait = new Concatenation("simpleSystem_0_mainBlock_pA1out_5Output_AgentOutputConstraint_OutputDef_Output_writeByWeight_FilterByDef_FilterBy_concatWait");
	Wait simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_readByWeight_FilterByDef_FilterBy_waitHead = new Wait(simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_readByWeight_FilterByDef_SeqHead,"simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_readByWeight_FilterByDef_FilterBy_waitHead");
	Concatenation simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredByExecTime_FilterByDef_FilterBy_concatWait = new Concatenation("simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredByExecTime_FilterByDef_FilterBy_concatWait");
	Death simpleSystem_0_mainBlock_pA1outState_6Output_AgentOutputConstraint_OutputDef_Output_writeByWeight_FilterByDef_killIt = new Death("simpleSystem_0_mainBlock_pA1outState_6Output_AgentOutputConstraint_OutputDef_Output_writeByWeight_FilterByDef_killIt");
	Concatenation simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_pushByWeightPlusOne_FilterByDef_FilterBy_concatWait = new Concatenation("simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_pushByWeightPlusOne_FilterByDef_FilterBy_concatWait");
	Death simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_pushByWeightPlusOne_FilterByDef_FilterBy_death = new Death("simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_pushByWeightPlusOne_FilterByDef_FilterBy_death");
	Union simpleSystem_0_mainBlock_CPU1_9intermediate_allAgentStops1 = new Union("simpleSystem_0_mainBlock_CPU1_9intermediate_allAgentStops1");
	Defer simpleSystem_0_mainBlock_A1_2Alternates_NonReentrantAgent_AlternatesDef_Alt_c1DelayedByOne = new Defer("simpleSystem_0_mainBlock_A1_2Alternates_NonReentrantAgent_AlternatesDef_Alt_c1DelayedByOne");
	Death simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredByExecTime_FilterByDef_killIt = new Death("simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredByExecTime_FilterByDef_killIt");
	Death simpleSystem_0_mainBlock_pA1out_5Output_AgentOutputConstraint_OutputDef_Output_writeByWeight_FilterByDef_killIt = new Death("simpleSystem_0_mainBlock_pA1out_5Output_AgentOutputConstraint_OutputDef_Output_writeByWeight_FilterByDef_killIt");
	Death simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_readByWeightPlusOne_FilterByDef_killIt = new Death("simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_readByWeightPlusOne_FilterByDef_killIt");
	Concatenation simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_readByWeightPlusOne_FilterByDef_FilterBy_concatWait = new Concatenation("simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_readByWeightPlusOne_FilterByDef_FilterBy_concatWait");
	Death simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredByExecTime_FilterByDef_killIt = new Death("simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredByExecTime_FilterByDef_killIt");
	Defer simpleSystem_0_mainBlock_A2_1Alternates_NonReentrantAgent_AlternatesDef_Alt_c1DelayedByOne = new Defer("simpleSystem_0_mainBlock_A2_1Alternates_NonReentrantAgent_AlternatesDef_Alt_c1DelayedByOne");
	Wait simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_readByWeightPlusOne_FilterByDef_FilterBy_waitHead = new Wait(simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_readByWeightPlusOne_FilterByDef_SeqHead,"simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_readByWeightPlusOne_FilterByDef_FilterBy_waitHead");
	Death simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredStartThenByExecTime_FilterByDef_killIt = new Death("simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredStartThenByExecTime_FilterByDef_killIt");
	Union simpleSystem_0_mainBlock_CPU1_9intermediate_allAgentsExecutions1 = new Union("simpleSystem_0_mainBlock_CPU1_9intermediate_allAgentsExecutions1");
	Death simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_pushByWeightPlusOne_FilterByDef_killIt = new Death("simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_pushByWeightPlusOne_FilterByDef_killIt");
	Death simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_readOneThenByWeightPlusOne_FilterByDef_FilterBy_death = new Death("simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_readOneThenByWeightPlusOne_FilterByDef_FilterBy_death");
	Defer simpleSystem_0_mainBlock_placeA1State_8Token_TokenConstraint_TokenDef_Token_readDelayedForDelay = new Defer("simpleSystem_0_mainBlock_placeA1State_8Token_TokenConstraint_TokenDef_Token_readDelayedForDelay");
	Concatenation simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_readByWeightPlusOne_FilterByDef_FilterBy_concatWait = new Concatenation("simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_readByWeightPlusOne_FilterByDef_FilterBy_concatWait");
	Death simpleSystem_0_mainBlock_pA1out_5Output_AgentOutputConstraint_OutputDef_Output_writeByWeight_FilterByDef_FilterBy_death = new Death("simpleSystem_0_mainBlock_pA1out_5Output_AgentOutputConstraint_OutputDef_Output_writeByWeight_FilterByDef_FilterBy_death");
	Union simpleSystem_0_mainBlock_mem1_10intermediate_allPortWrites1 = new Union("simpleSystem_0_mainBlock_mem1_10intermediate_allPortWrites1");
	Death simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredStartThenByExecTime_FilterByDef_FilterBy_death = new Death("simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredStartThenByExecTime_FilterByDef_FilterBy_death");
	Death simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_readOneThenByWeightPlusOne_FilterByDef_killIt = new Death("simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_readOneThenByWeightPlusOne_FilterByDef_killIt");
	Death simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_readOneThenByWeightPlusOne_FilterByDef_FilterBy_death = new Death("simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_readOneThenByWeightPlusOne_FilterByDef_FilterBy_death");
	Death simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_readByWeight_FilterByDef_FilterBy_death = new Death("simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_readByWeight_FilterByDef_FilterBy_death");
	Concatenation simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredStartThenByExecTime_FilterByDef_FilterBy_concatWait = new Concatenation("simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredStartThenByExecTime_FilterByDef_FilterBy_concatWait");
	Wait simpleSystem_0_mainBlock_pA1out_5Output_AgentOutputConstraint_OutputDef_Output_writeByWeight_FilterByDef_FilterBy_waitHead = new Wait(simpleSystem_0_mainBlock_pA1out_5Output_AgentOutputConstraint_OutputDef_Output_writeByWeight_FilterByDef_SeqHead,"simpleSystem_0_mainBlock_pA1out_5Output_AgentOutputConstraint_OutputDef_Output_writeByWeight_FilterByDef_FilterBy_waitHead");
	Wait simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredByExecTime_FilterByDef_FilterBy_waitHead = new Wait(simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredByExecTime_FilterByDef_SeqHead,"simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredByExecTime_FilterByDef_FilterBy_waitHead");
	Wait simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_pushByWeightPlusOne_FilterByDef_FilterBy_waitHead = new Wait(simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_pushByWeightPlusOne_FilterByDef_SeqHead,"simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_pushByWeightPlusOne_FilterByDef_FilterBy_waitHead");
	Defer simpleSystem_0_mainBlock_place1_7_DelayFor_NwriteInAdvance_DelayForDef_DelayFor_defer = new Defer("simpleSystem_0_mainBlock_place1_7_DelayFor_NwriteInAdvance_DelayForDef_DelayFor_defer");
	Concatenation simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_readOneThenByWeightPlusOne_FilterByDef_FilterBy_concatWait = new Concatenation("simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_readOneThenByWeightPlusOne_FilterByDef_FilterBy_concatWait");
	Wait simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_readByWeightPlusOne_FilterByDef_FilterBy_waitHead = new Wait(simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_readByWeightPlusOne_FilterByDef_SeqHead,"simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_readByWeightPlusOne_FilterByDef_FilterBy_waitHead");
	Death simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredStartThenByExecTime_FilterByDef_killIt = new Death("simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredStartThenByExecTime_FilterByDef_killIt");
	Wait simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredStartThenByExecTime_FilterByDef_FilterBy_waitHead = new Wait(simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredStartThenByExecTime_FilterByDef_SeqHead,"simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredStartThenByExecTime_FilterByDef_FilterBy_waitHead");
	Death simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_readByWeight_FilterByDef_killIt = new Death("simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_readByWeight_FilterByDef_killIt");
	Concatenation simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_pushByWeightPlusOne_FilterByDef_FilterBy_concatWait = new Concatenation("simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_pushByWeightPlusOne_FilterByDef_FilterBy_concatWait");
	Concatenation simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_readOneThenByWeightPlusOne_FilterByDef_FilterBy_concatWait = new Concatenation("simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_readOneThenByWeightPlusOne_FilterByDef_FilterBy_concatWait");
	Union simpleSystem_0_mainBlock_mem1_10intermediate_allPortReads1 = new Union("simpleSystem_0_mainBlock_mem1_10intermediate_allPortReads1");
	Wait simpleSystem_0_mainBlock_pA1outState_6Output_AgentOutputConstraint_OutputDef_Output_writeByWeight_FilterByDef_FilterBy_waitHead = new Wait(simpleSystem_0_mainBlock_pA1outState_6Output_AgentOutputConstraint_OutputDef_Output_writeByWeight_FilterByDef_SeqHead,"simpleSystem_0_mainBlock_pA1outState_6Output_AgentOutputConstraint_OutputDef_Output_writeByWeight_FilterByDef_FilterBy_waitHead");
	Death simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredByExecTime_FilterByDef_FilterBy_death = new Death("simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredByExecTime_FilterByDef_FilterBy_death");
	Wait simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_readByWeight_FilterByDef_FilterBy_waitHead = new Wait(simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_readByWeight_FilterByDef_SeqHead,"simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_readByWeight_FilterByDef_FilterBy_waitHead");
	Defer simpleSystem_0_mainBlock_CPU1_9Alternates_NonPreemptiveExecution_AlternatesDef_Alt_c1DelayedByOne = new Defer("simpleSystem_0_mainBlock_CPU1_9Alternates_NonPreemptiveExecution_AlternatesDef_Alt_c1DelayedByOne");
	Death simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredByExecTime_FilterByDef_FilterBy_death = new Death("simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredByExecTime_FilterByDef_FilterBy_death");
	Wait simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredStartThenByExecTime_FilterByDef_FilterBy_waitHead = new Wait(simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredStartThenByExecTime_FilterByDef_SeqHead,"simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredStartThenByExecTime_FilterByDef_FilterBy_waitHead");
	Wait simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_readOneThenByWeightPlusOne_FilterByDef_FilterBy_waitHead = new Wait(simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_readOneThenByWeightPlusOne_FilterByDef_SeqHead,"simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_readOneThenByWeightPlusOne_FilterByDef_FilterBy_waitHead");
	Wait simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_pushByWeightPlusOne_FilterByDef_FilterBy_waitHead = new Wait(simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_pushByWeightPlusOne_FilterByDef_SeqHead,"simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_pushByWeightPlusOne_FilterByDef_FilterBy_waitHead");
	Death simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_pushByWeightPlusOne_FilterByDef_FilterBy_death = new Death("simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_pushByWeightPlusOne_FilterByDef_FilterBy_death");
	Death simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_readByWeight_FilterByDef_FilterBy_death = new Death("simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_readByWeight_FilterByDef_FilterBy_death");
	Concatenation simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredStartThenByExecTime_FilterByDef_FilterBy_concatWait = new Concatenation("simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredStartThenByExecTime_FilterByDef_FilterBy_concatWait");
	Defer simpleSystem_0_mainBlock_place1_7Token_TokenConstraint_TokenDef_Token_readDelayedForDelay = new Defer("simpleSystem_0_mainBlock_place1_7Token_TokenConstraint_TokenDef_Token_readDelayedForDelay");
	Death simpleSystem_0_mainBlock_pA1outState_6Output_AgentOutputConstraint_OutputDef_Output_writeByWeight_FilterByDef_FilterBy_death = new Death("simpleSystem_0_mainBlock_pA1outState_6Output_AgentOutputConstraint_OutputDef_Output_writeByWeight_FilterByDef_FilterBy_death");
	Union simpleSystem_0_mainBlock_CPU1_9intermediate_allAgentStarts1 = new Union("simpleSystem_0_mainBlock_CPU1_9intermediate_allAgentStarts1");
	Concatenation simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredByExecTime_FilterByDef_FilterBy_concatWait = new Concatenation("simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredByExecTime_FilterByDef_FilterBy_concatWait");
	Death simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_readByWeightPlusOne_FilterByDef_FilterBy_death = new Death("simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_readByWeightPlusOne_FilterByDef_FilterBy_death");
	Concatenation simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_readByWeight_FilterByDef_FilterBy_concatWait = new Concatenation("simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_readByWeight_FilterByDef_FilterBy_concatWait");
	Wait simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_readOneThenByWeightPlusOne_FilterByDef_FilterBy_waitHead = new Wait(simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_readOneThenByWeightPlusOne_FilterByDef_SeqHead,"simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_readOneThenByWeightPlusOne_FilterByDef_FilterBy_waitHead");
	Death simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_readByWeightPlusOne_FilterByDef_killIt = new Death("simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_readByWeightPlusOne_FilterByDef_killIt");
	Death simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_readByWeightPlusOne_FilterByDef_FilterBy_death = new Death("simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_readByWeightPlusOne_FilterByDef_FilterBy_death");
	Death simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredStartThenByExecTime_FilterByDef_FilterBy_death = new Death("simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredStartThenByExecTime_FilterByDef_FilterBy_death");
	 ConditionalExpression simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_readByWeight_FilterByDef_FilterBy_FilterBySeqTail = new ConditionalExpression("simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_readByWeight_FilterByDef_FilterBy_FilterBySeqTail");
	 ConditionalExpression simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_readByWeightPlusOne = new ConditionalExpression("simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_readByWeightPlusOne");
	 ConditionalExpression simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_readOneThenByWeightPlusOne = new ConditionalExpression("simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_readOneThenByWeightPlusOne");
	 ConditionalExpression simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredStartThenByExecTime = new ConditionalExpression("simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredStartThenByExecTime");
	 ConditionalExpression simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_pushByWeightPlusOne = new ConditionalExpression("simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_pushByWeightPlusOne");
	 ConditionalExpression simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_readByWeight = new ConditionalExpression("simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_readByWeight");
	 ConditionalExpression simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredByExecTime_FilterByDef_FilterBy_FilterBySeqTail = new ConditionalExpression("simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredByExecTime_FilterByDef_FilterBy_FilterBySeqTail");
	 ConditionalExpression simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_readOneThenByWeightPlusOne_FilterByDef_FilterBy_FilterBySeqTail = new ConditionalExpression("simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_readOneThenByWeightPlusOne_FilterByDef_FilterBy_FilterBySeqTail");
	 ConditionalExpression simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_pushByWeightPlusOne_FilterByDef_FilterBy_FilterBySeqTail = new ConditionalExpression("simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_pushByWeightPlusOne_FilterByDef_FilterBy_FilterBySeqTail");
	 UserDefinedExpression simpleSystem_0_mainBlock_placeA1State_8_DelayFor_NwriteInAdvance = new UserDefinedExpression("simpleSystem_0_mainBlock_placeA1State_8_DelayFor_NwriteInAdvance");
	 ConditionalExpression simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredByExecTime_FilterByDef_FilterBy_FilterBySeqTail = new ConditionalExpression("simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredByExecTime_FilterByDef_FilterBy_FilterBySeqTail");
	 ConditionalExpression simpleSystem_0_mainBlock_pA1out_5Output_AgentOutputConstraint_OutputDef_Output_writeByWeight_FilterByDef_FilterBy_FilterBySeqTail = new ConditionalExpression("simpleSystem_0_mainBlock_pA1out_5Output_AgentOutputConstraint_OutputDef_Output_writeByWeight_FilterByDef_FilterBy_FilterBySeqTail");
	 ConditionalExpression simpleSystem_0_mainBlock_pA1outState_6Output_AgentOutputConstraint_OutputDef_Output_writeByWeight = new ConditionalExpression("simpleSystem_0_mainBlock_pA1outState_6Output_AgentOutputConstraint_OutputDef_Output_writeByWeight");
	 ConditionalExpression simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_readOneThenByWeightPlusOne_FilterByDef_FilterBy_FilterBySeqTail = new ConditionalExpression("simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_readOneThenByWeightPlusOne_FilterByDef_FilterBy_FilterBySeqTail");
	 ConditionalExpression simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_readByWeight_FilterByDef_FilterBy_FilterBySeqTail = new ConditionalExpression("simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_readByWeight_FilterByDef_FilterBy_FilterBySeqTail");
	 ConditionalExpression simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredByExecTime = new ConditionalExpression("simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredByExecTime");
	 ConditionalExpression simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_pushByWeightPlusOne = new ConditionalExpression("simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_pushByWeightPlusOne");
	 UserDefinedExpression simpleSystem_0_mainBlock_place1_7_DelayFor_NwriteInAdvance = new UserDefinedExpression("simpleSystem_0_mainBlock_place1_7_DelayFor_NwriteInAdvance");
	 ConditionalExpression simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredByExecTime = new ConditionalExpression("simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredByExecTime");
	 ConditionalExpression simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_readByWeightPlusOne_FilterByDef_FilterBy_FilterBySeqTail = new ConditionalExpression("simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_readByWeightPlusOne_FilterByDef_FilterBy_FilterBySeqTail");
	 ConditionalExpression simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_readOneThenByWeightPlusOne = new ConditionalExpression("simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_readOneThenByWeightPlusOne");
	 ConditionalExpression simpleSystem_0_mainBlock_pA1out_5Output_AgentOutputConstraint_OutputDef_Output_writeByWeight = new ConditionalExpression("simpleSystem_0_mainBlock_pA1out_5Output_AgentOutputConstraint_OutputDef_Output_writeByWeight");
	 ConditionalExpression simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredStartThenByExecTime_FilterByDef_FilterBy_FilterBySeqTail = new ConditionalExpression("simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredStartThenByExecTime_FilterByDef_FilterBy_FilterBySeqTail");
	 ConditionalExpression simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredStartThenByExecTime_FilterByDef_FilterBy_FilterBySeqTail = new ConditionalExpression("simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredStartThenByExecTime_FilterByDef_FilterBy_FilterBySeqTail");
	 ConditionalExpression simpleSystem_0_mainBlock_pA1outState_6Output_AgentOutputConstraint_OutputDef_Output_writeByWeight_FilterByDef_FilterBy_FilterBySeqTail = new ConditionalExpression("simpleSystem_0_mainBlock_pA1outState_6Output_AgentOutputConstraint_OutputDef_Output_writeByWeight_FilterByDef_FilterBy_FilterBySeqTail");
	 ConditionalExpression simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_pushByWeightPlusOne_FilterByDef_FilterBy_FilterBySeqTail = new ConditionalExpression("simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_pushByWeightPlusOne_FilterByDef_FilterBy_FilterBySeqTail");
	 ConditionalExpression simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredStartThenByExecTime = new ConditionalExpression("simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredStartThenByExecTime");
	 ConditionalExpression simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_readByWeightPlusOne_FilterByDef_FilterBy_FilterBySeqTail = new ConditionalExpression("simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_readByWeightPlusOne_FilterByDef_FilterBy_FilterBySeqTail");
	 ConditionalExpression simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_readByWeight = new ConditionalExpression("simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_readByWeight");
	 ConditionalExpression simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_readByWeightPlusOne = new ConditionalExpression("simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_readByWeightPlusOne");
	Exclusion simpleSystem_0_mainBlock_mem1_10Exclusion_IsReadingXorWriting = new Exclusion(simpleSystem_0_mainBlock_mem1_10_isWriting,simpleSystem_0_mainBlock_mem1_10_isReading);
	Exclusion simpleSystem_0_mainBlock_CPU1_9intermediate_oneAgentAtATime1_2 = new Exclusion(simpleSystem_0_mainBlock_A2_1_isExecuting,simpleSystem_0_mainBlock_A1_2_isExecuting);
	Exclusion simpleSystem_0_mainBlock_CPU1_9Exclusion_IsExecutingXorIdle = new Exclusion(simpleSystem_0_mainBlock_CPU1_9_idle,simpleSystem_0_mainBlock_CPU1_9_isExecuting);
	Coincides simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_stopAfterExecTime = new Coincides(simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredByExecTime,simpleSystem_0_mainBlock_A2_1_stopAgent);
	Precedes simpleSystem_0_mainBlock_A1_2Alternates_NonReentrantAgent_AlternatesDef_Alt_c2precedesC1DelayedByOne = new Precedes(simpleSystem_0_mainBlock_A1_2_stopAgent,simpleSystem_0_mainBlock_A1_2Alternates_NonReentrantAgent_AlternatesDef_Alt_c1DelayedByOne);
	Coincides simpleSystem_0_mainBlock_pA1out_5Coincides_portWriteInPlace = new Coincides(simpleSystem_0_mainBlock_place1_7_push,simpleSystem_0_mainBlock_pA1out_5_write);
	Precedes simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_weightTokenCausesExec = new Precedes(simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_pushByWeightPlusOne,simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_readOneThenByWeightPlusOne);
	Precedes simpleSystem_0_mainBlock_placeA1State_8Token_TokenConstraint_TokenDef_Token_causalityImmediate1 = new Precedes(simpleSystem_0_mainBlock_pA1outState_6_write,simpleSystem_0_mainBlock_pA1inState_3_read);
	Precedes simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_weightPlusOneTokenCausesExec = new Precedes(simpleSystem_0_mainBlock_A1_2_startAgent,simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_readByWeightPlusOne);
	Coincides simpleSystem_0_mainBlock_pA1outState_6Coincides_portWriteInPlace = new Coincides(simpleSystem_0_mainBlock_placeA1State_8_push,simpleSystem_0_mainBlock_pA1outState_6_write);
	Precedes simpleSystem_0_mainBlock_placeA1State_8Precedes_BoundedPlaces = new Precedes(simpleSystem_0_mainBlock_pA1inState_3_read,simpleSystem_0_mainBlock_placeA1State_8_DelayFor_NwriteInAdvance);
	Precedes simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_weightPlusOneTokenCausesExec = new Precedes(simpleSystem_0_mainBlock_A2_1_startAgent,simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_readByWeightPlusOne);
	Precedes simpleSystem_0_mainBlock_A1_2Alternates_NonReentrantAgent_AlternatesDef_Alt_c1PrecedesC2 = new Precedes(simpleSystem_0_mainBlock_A1_2_startAgent,simpleSystem_0_mainBlock_A1_2_stopAgent);
	Coincides simpleSystem_0_mainBlock_placeA1State_8Token_TokenConstraint_TokenDef_Token_causalityDelayed = new Coincides(simpleSystem_0_mainBlock_pA1outState_6_write,simpleSystem_0_mainBlock_pA1outState_6_write);
	Coincides simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_stopAfterExecTime2 = new Coincides(simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredStartThenByExecTime,simpleSystem_0_mainBlock_A2_1_startAgent);
	Coincides simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_stopAfterExecTime = new Coincides(simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredByExecTime,simpleSystem_0_mainBlock_A1_2_stopAgent);
	Precedes simpleSystem_0_mainBlock_placeA1State_8Token_TokenConstraint_TokenDef_Token_causalityImmediate2 = new Precedes(simpleSystem_0_mainBlock_pA1outState_6_write,simpleSystem_0_mainBlock_placeA1State_8Token_TokenConstraint_TokenDef_Token_readDelayedForDelay);
	Coincides simpleSystem_0_mainBlock_pA1outState_6Output_AgentOutputConstraint_OutputDef_Output_ExecWriteFirstToken = new Coincides(simpleSystem_0_mainBlock_A1_2_stopAgent,simpleSystem_0_mainBlock_pA1outState_6Output_AgentOutputConstraint_OutputDef_Output_writeByWeight);
	Coincides simpleSystem_0_mainBlock_pA2in_4Coincides_portReadInPlace = new Coincides(simpleSystem_0_mainBlock_place1_7_pop,simpleSystem_0_mainBlock_pA2in_4_read);
	Precedes simpleSystem_0_mainBlock_CPU1_9Alternates_NonPreemptiveExecution_AlternatesDef_Alt_c2precedesC1DelayedByOne = new Precedes(simpleSystem_0_mainBlock_CPU1_9intermediate_allAgentStops1,simpleSystem_0_mainBlock_CPU1_9Alternates_NonPreemptiveExecution_AlternatesDef_Alt_c1DelayedByOne);
	Precedes simpleSystem_0_mainBlock_A2_1Alternates_NonReentrantAgent_AlternatesDef_Alt_c2precedesC1DelayedByOne = new Precedes(simpleSystem_0_mainBlock_A2_1_stopAgent,simpleSystem_0_mainBlock_A2_1Alternates_NonReentrantAgent_AlternatesDef_Alt_c1DelayedByOne);
	Precedes simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_weightTokenCausesExec = new Precedes(simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_readByWeight,simpleSystem_0_mainBlock_A1_2_startAgent);
	Precedes simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_weightTokenCausesExec = new Precedes(simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_readByWeight,simpleSystem_0_mainBlock_A2_1_startAgent);
	Precedes simpleSystem_0_mainBlock_place1_7Precedes_BoundedPlaces = new Precedes(simpleSystem_0_mainBlock_pA2in_4_read,simpleSystem_0_mainBlock_place1_7_DelayFor_NwriteInAdvance);
	Coincides simpleSystem_0_mainBlock_place1_7Token_TokenConstraint_TokenDef_Token_causalityDelayed = new Coincides(simpleSystem_0_mainBlock_pA1out_5_write,simpleSystem_0_mainBlock_pA1out_5_write);
	Precedes simpleSystem_0_mainBlock_A2_1Alternates_NonReentrantAgent_AlternatesDef_Alt_c1PrecedesC2 = new Precedes(simpleSystem_0_mainBlock_A2_1_startAgent,simpleSystem_0_mainBlock_A2_1_stopAgent);
	Coincides simpleSystem_0_mainBlock_CPU1_9Coincides_ExecutesItsAllocatedAgents = new Coincides(simpleSystem_0_mainBlock_CPU1_9intermediate_allAgentsExecutions1,simpleSystem_0_mainBlock_CPU1_9_isExecuting);
	Precedes simpleSystem_0_mainBlock_CPU1_9Alternates_NonPreemptiveExecution_AlternatesDef_Alt_c1PrecedesC2 = new Precedes(simpleSystem_0_mainBlock_CPU1_9intermediate_allAgentStarts1,simpleSystem_0_mainBlock_CPU1_9intermediate_allAgentStops1);
	Coincides simpleSystem_0_mainBlock_pA1out_5Output_AgentOutputConstraint_OutputDef_Output_ExecWriteFirstToken = new Coincides(simpleSystem_0_mainBlock_A1_2_stopAgent,simpleSystem_0_mainBlock_pA1out_5Output_AgentOutputConstraint_OutputDef_Output_writeByWeight);
	Coincides simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_stopAfterExecTime2 = new Coincides(simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredStartThenByExecTime,simpleSystem_0_mainBlock_A1_2_startAgent);
	Precedes simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_weightTokenCausesExec = new Precedes(simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_pushByWeightPlusOne,simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_readOneThenByWeightPlusOne);
	Coincides simpleSystem_0_mainBlock_mem1_10Coincides_isWritingAccordingToPortWrite = new Coincides(simpleSystem_0_mainBlock_mem1_10intermediate_allPortWrites1,simpleSystem_0_mainBlock_mem1_10_isWriting);
	Coincides simpleSystem_0_mainBlock_mem1_10Coincides_isReadingAccordingToPortRead = new Coincides(simpleSystem_0_mainBlock_mem1_10intermediate_allPortReads1,simpleSystem_0_mainBlock_mem1_10_isReading);
	Precedes simpleSystem_0_mainBlock_place1_7Token_TokenConstraint_TokenDef_Token_causalityImmediate2 = new Precedes(simpleSystem_0_mainBlock_pA1out_5_write,simpleSystem_0_mainBlock_place1_7Token_TokenConstraint_TokenDef_Token_readDelayedForDelay);
	Precedes simpleSystem_0_mainBlock_place1_7Token_TokenConstraint_TokenDef_Token_causalityImmediate1 = new Precedes(simpleSystem_0_mainBlock_pA1out_5_write,simpleSystem_0_mainBlock_pA2in_4_read);
	Coincides simpleSystem_0_mainBlock_pA1inState_3Coincides_portReadInPlace = new Coincides(simpleSystem_0_mainBlock_placeA1State_8_pop,simpleSystem_0_mainBlock_pA1inState_3_read);
	 ConditionalRelation simpleSystem_0_mainBlock_place1_7Token_TokenConstraint = new ConditionalRelation();
	 ConditionalRelation simpleSystem_0_mainBlock_placeA1State_8Token_TokenConstraint = new ConditionalRelation();
simpleSystem_0_mainBlock_place1_7Token_TokenConstraint_TokenDef_Token_causalityImmediate2.isActive = false;
simpleSystem_0_mainBlock_place1_7Token_TokenConstraint_TokenDef_Token_causalityDelayed.isActive = false;
simpleSystem_0_mainBlock_placeA1State_8Token_TokenConstraint_TokenDef_Token_causalityImmediate1.isActive = false;
simpleSystem_0_mainBlock_placeA1State_8Token_TokenConstraint_TokenDef_Token_causalityDelayed.isActive = false;
	simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_readByWeight_FilterByDef_FilterBy_concatWait.connect(simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_readByWeight_FilterByDef_FilterBy_waitHead,simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_readByWeight_FilterByDef_FilterBy_FilterBySeqTail, true, simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_readByWeight_FilterByDef_SeqTail);
	simpleSystem_0_mainBlock_pA1outState_6Output_AgentOutputConstraint_OutputDef_Output_writeByWeight_FilterByDef_FilterBy_concatWait.connect(simpleSystem_0_mainBlock_pA1outState_6Output_AgentOutputConstraint_OutputDef_Output_writeByWeight_FilterByDef_FilterBy_waitHead,simpleSystem_0_mainBlock_pA1outState_6Output_AgentOutputConstraint_OutputDef_Output_writeByWeight_FilterByDef_FilterBy_FilterBySeqTail, true, simpleSystem_0_mainBlock_pA1outState_6Output_AgentOutputConstraint_OutputDef_Output_writeByWeight_FilterByDef_SeqTail);
	simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredByExecTime_FilterByDef_FilterBy_waitHead.connect(simpleSystem_0_mainBlock_A2_1_isExecuting);
	simpleSystem_0_mainBlock_placeA1State_8_DelayFor_NwriteInAdvance_DelayForDef_DelayFor_defer.connect(simpleSystem_0_mainBlock_pA1outState_6_write,simpleSystem_0_mainBlock_pA1outState_6_write,simpleSystem_0_mainBlock_placeA1State_8_DelayFor_NwriteInAdvance_DelayForDef_seqForDelayFor);
	simpleSystem_0_mainBlock_pA1out_5Output_AgentOutputConstraint_OutputDef_Output_writeByWeight_FilterByDef_FilterBy_concatWait.connect(simpleSystem_0_mainBlock_pA1out_5Output_AgentOutputConstraint_OutputDef_Output_writeByWeight_FilterByDef_FilterBy_waitHead,simpleSystem_0_mainBlock_pA1out_5Output_AgentOutputConstraint_OutputDef_Output_writeByWeight_FilterByDef_FilterBy_FilterBySeqTail, true, simpleSystem_0_mainBlock_pA1out_5Output_AgentOutputConstraint_OutputDef_Output_writeByWeight_FilterByDef_SeqTail);
	simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_readByWeight_FilterByDef_FilterBy_waitHead.connect(simpleSystem_0_mainBlock_pA2in_4_read);
	simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredByExecTime_FilterByDef_FilterBy_concatWait.connect(simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredByExecTime_FilterByDef_FilterBy_waitHead,simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredByExecTime_FilterByDef_FilterBy_FilterBySeqTail, true, simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredByExecTime_FilterByDef_SeqTail);
	simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_pushByWeightPlusOne_FilterByDef_FilterBy_concatWait.connect(simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_pushByWeightPlusOne_FilterByDef_FilterBy_waitHead,simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_pushByWeightPlusOne_FilterByDef_FilterBy_FilterBySeqTail, true, simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_pushByWeightPlusOne_FilterByDef_SeqTail);
	simpleSystem_0_mainBlock_CPU1_9intermediate_allAgentStops1.connect(simpleSystem_0_mainBlock_A1_2_stopAgent,simpleSystem_0_mainBlock_A2_1_stopAgent);
	simpleSystem_0_mainBlock_A1_2Alternates_NonReentrantAgent_AlternatesDef_Alt_c1DelayedByOne.connect(simpleSystem_0_mainBlock_A1_2_startAgent,simpleSystem_0_mainBlock_A1_2_startAgent,simpleSystem_0_mainBlock_A1_2Alternates_NonReentrantAgent_AlternatesDef_seqOneInfinite);
	simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_readByWeightPlusOne_FilterByDef_FilterBy_concatWait.connect(simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_readByWeightPlusOne_FilterByDef_FilterBy_waitHead,simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_readByWeightPlusOne_FilterByDef_FilterBy_FilterBySeqTail, true, simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_readByWeightPlusOne_FilterByDef_SeqTail);
	simpleSystem_0_mainBlock_A2_1Alternates_NonReentrantAgent_AlternatesDef_Alt_c1DelayedByOne.connect(simpleSystem_0_mainBlock_A2_1_startAgent,simpleSystem_0_mainBlock_A2_1_startAgent,simpleSystem_0_mainBlock_A2_1Alternates_NonReentrantAgent_AlternatesDef_seqOneInfinite);
	simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_readByWeightPlusOne_FilterByDef_FilterBy_waitHead.connect(simpleSystem_0_mainBlock_pA1inState_3_read);
	simpleSystem_0_mainBlock_CPU1_9intermediate_allAgentsExecutions1.connect(simpleSystem_0_mainBlock_A1_2_isExecuting,simpleSystem_0_mainBlock_A2_1_isExecuting);
	simpleSystem_0_mainBlock_placeA1State_8Token_TokenConstraint_TokenDef_Token_readDelayedForDelay.connect(simpleSystem_0_mainBlock_pA1inState_3_read,simpleSystem_0_mainBlock_pA1inState_3_read,simpleSystem_0_mainBlock_placeA1State_8Token_TokenConstraint_TokenDef_sDelay);
	simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_readByWeightPlusOne_FilterByDef_FilterBy_concatWait.connect(simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_readByWeightPlusOne_FilterByDef_FilterBy_waitHead,simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_readByWeightPlusOne_FilterByDef_FilterBy_FilterBySeqTail, true, simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_readByWeightPlusOne_FilterByDef_SeqTail);
	simpleSystem_0_mainBlock_mem1_10intermediate_allPortWrites1.connect(simpleSystem_0_mainBlock_pA1out_5_write,simpleSystem_0_mainBlock_pA1out_5_write);
	simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredStartThenByExecTime_FilterByDef_FilterBy_concatWait.connect(simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredStartThenByExecTime_FilterByDef_FilterBy_waitHead,simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredStartThenByExecTime_FilterByDef_FilterBy_FilterBySeqTail, true, simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredStartThenByExecTime_FilterByDef_SeqTail);
	simpleSystem_0_mainBlock_pA1out_5Output_AgentOutputConstraint_OutputDef_Output_writeByWeight_FilterByDef_FilterBy_waitHead.connect(simpleSystem_0_mainBlock_pA1out_5_write);
	simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredByExecTime_FilterByDef_FilterBy_waitHead.connect(simpleSystem_0_mainBlock_A1_2_isExecuting);
	simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_pushByWeightPlusOne_FilterByDef_FilterBy_waitHead.connect(simpleSystem_0_mainBlock_place1_7_push);
	simpleSystem_0_mainBlock_place1_7_DelayFor_NwriteInAdvance_DelayForDef_DelayFor_defer.connect(simpleSystem_0_mainBlock_pA1out_5_write,simpleSystem_0_mainBlock_pA1out_5_write,simpleSystem_0_mainBlock_place1_7_DelayFor_NwriteInAdvance_DelayForDef_seqForDelayFor);
	simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_readOneThenByWeightPlusOne_FilterByDef_FilterBy_concatWait.connect(simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_readOneThenByWeightPlusOne_FilterByDef_FilterBy_waitHead,simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_readOneThenByWeightPlusOne_FilterByDef_FilterBy_FilterBySeqTail, true, simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_readOneThenByWeightPlusOne_FilterByDef_SeqTail);
	simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_readByWeightPlusOne_FilterByDef_FilterBy_waitHead.connect(simpleSystem_0_mainBlock_pA2in_4_read);
	simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredStartThenByExecTime_FilterByDef_FilterBy_waitHead.connect(simpleSystem_0_mainBlock_A2_1_isExecuting);
	simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_pushByWeightPlusOne_FilterByDef_FilterBy_concatWait.connect(simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_pushByWeightPlusOne_FilterByDef_FilterBy_waitHead,simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_pushByWeightPlusOne_FilterByDef_FilterBy_FilterBySeqTail, true, simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_pushByWeightPlusOne_FilterByDef_SeqTail);
	simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_readOneThenByWeightPlusOne_FilterByDef_FilterBy_concatWait.connect(simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_readOneThenByWeightPlusOne_FilterByDef_FilterBy_waitHead,simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_readOneThenByWeightPlusOne_FilterByDef_FilterBy_FilterBySeqTail, true, simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_readOneThenByWeightPlusOne_FilterByDef_SeqTail);
	simpleSystem_0_mainBlock_mem1_10intermediate_allPortReads1.connect(simpleSystem_0_mainBlock_pA2in_4_read,simpleSystem_0_mainBlock_pA2in_4_read);
	simpleSystem_0_mainBlock_pA1outState_6Output_AgentOutputConstraint_OutputDef_Output_writeByWeight_FilterByDef_FilterBy_waitHead.connect(simpleSystem_0_mainBlock_pA1outState_6_write);
	simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_readByWeight_FilterByDef_FilterBy_waitHead.connect(simpleSystem_0_mainBlock_pA1inState_3_read);
	simpleSystem_0_mainBlock_CPU1_9Alternates_NonPreemptiveExecution_AlternatesDef_Alt_c1DelayedByOne.connect(simpleSystem_0_mainBlock_CPU1_9intermediate_allAgentStarts1,simpleSystem_0_mainBlock_CPU1_9intermediate_allAgentStarts1,simpleSystem_0_mainBlock_CPU1_9Alternates_NonPreemptiveExecution_AlternatesDef_seqOneInfinite);
	simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredStartThenByExecTime_FilterByDef_FilterBy_waitHead.connect(simpleSystem_0_mainBlock_A1_2_isExecuting);
	simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_readOneThenByWeightPlusOne_FilterByDef_FilterBy_waitHead.connect(simpleSystem_0_mainBlock_pA2in_4_read);
	simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_pushByWeightPlusOne_FilterByDef_FilterBy_waitHead.connect(simpleSystem_0_mainBlock_placeA1State_8_push);
	simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredStartThenByExecTime_FilterByDef_FilterBy_concatWait.connect(simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredStartThenByExecTime_FilterByDef_FilterBy_waitHead,simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredStartThenByExecTime_FilterByDef_FilterBy_FilterBySeqTail, true, simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredStartThenByExecTime_FilterByDef_SeqTail);
	simpleSystem_0_mainBlock_place1_7Token_TokenConstraint_TokenDef_Token_readDelayedForDelay.connect(simpleSystem_0_mainBlock_pA2in_4_read,simpleSystem_0_mainBlock_pA2in_4_read,simpleSystem_0_mainBlock_place1_7Token_TokenConstraint_TokenDef_sDelay);
	simpleSystem_0_mainBlock_CPU1_9intermediate_allAgentStarts1.connect(simpleSystem_0_mainBlock_A1_2_startAgent,simpleSystem_0_mainBlock_A2_1_startAgent);
	simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredByExecTime_FilterByDef_FilterBy_concatWait.connect(simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredByExecTime_FilterByDef_FilterBy_waitHead,simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredByExecTime_FilterByDef_FilterBy_FilterBySeqTail, true, simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredByExecTime_FilterByDef_SeqTail);
	simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_readByWeight_FilterByDef_FilterBy_concatWait.connect(simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_readByWeight_FilterByDef_FilterBy_waitHead,simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_readByWeight_FilterByDef_FilterBy_FilterBySeqTail, true, simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_readByWeight_FilterByDef_SeqTail);
	simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_readOneThenByWeightPlusOne_FilterByDef_FilterBy_waitHead.connect(simpleSystem_0_mainBlock_pA1inState_3_read);
		simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_readByWeight_FilterByDef_FilterBy_FilterBySeqTail.connect(new ArrayList<Case>(Arrays.asList()));
		simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_readByWeightPlusOne.connect(new ArrayList<Case>(Arrays.asList(
			new Case(simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_readByWeightPlusOne_FilterByDef_FilterBy_death, simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_readByWeightPlusOne_FilterByDef_50),
			new Case(simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_readByWeightPlusOne_FilterByDef_FilterBy_concatWait, simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_readByWeightPlusOne_FilterByDef_70))));
		simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_readOneThenByWeightPlusOne.connect(new ArrayList<Case>(Arrays.asList(
			new Case(simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_readOneThenByWeightPlusOne_FilterByDef_FilterBy_death, simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_readOneThenByWeightPlusOne_FilterByDef_49),
			new Case(simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_readOneThenByWeightPlusOne_FilterByDef_FilterBy_concatWait, simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_readOneThenByWeightPlusOne_FilterByDef_56))));
		simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredStartThenByExecTime.connect(new ArrayList<Case>(Arrays.asList(
			new Case(simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredStartThenByExecTime_FilterByDef_FilterBy_death, simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredStartThenByExecTime_FilterByDef_63),
			new Case(simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredStartThenByExecTime_FilterByDef_FilterBy_concatWait, simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredStartThenByExecTime_FilterByDef_66))));
		simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_pushByWeightPlusOne.connect(new ArrayList<Case>(Arrays.asList(
			new Case(simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_pushByWeightPlusOne_FilterByDef_FilterBy_death, simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_pushByWeightPlusOne_FilterByDef_71),
			new Case(simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_pushByWeightPlusOne_FilterByDef_FilterBy_concatWait, simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_pushByWeightPlusOne_FilterByDef_68))));
		simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_readByWeight.connect(new ArrayList<Case>(Arrays.asList(
			new Case(simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_readByWeight_FilterByDef_FilterBy_death, simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_readByWeight_FilterByDef_51),
			new Case(simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_readByWeight_FilterByDef_FilterBy_concatWait, simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_readByWeight_FilterByDef_45))));
		simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredByExecTime_FilterByDef_FilterBy_FilterBySeqTail.connect(new ArrayList<Case>(Arrays.asList()));
		simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_readOneThenByWeightPlusOne_FilterByDef_FilterBy_FilterBySeqTail.connect(new ArrayList<Case>(Arrays.asList()));
		simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_pushByWeightPlusOne_FilterByDef_FilterBy_FilterBySeqTail.connect(new ArrayList<Case>(Arrays.asList()));
		simpleSystem_0_mainBlock_placeA1State_8_DelayFor_NwriteInAdvance.connect(simpleSystem_0_mainBlock_placeA1State_8_DelayFor_NwriteInAdvance_DelayForDef_DelayFor_defer);
		simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredByExecTime_FilterByDef_FilterBy_FilterBySeqTail.connect(new ArrayList<Case>(Arrays.asList()));
		simpleSystem_0_mainBlock_pA1out_5Output_AgentOutputConstraint_OutputDef_Output_writeByWeight_FilterByDef_FilterBy_FilterBySeqTail.connect(new ArrayList<Case>(Arrays.asList()));
		simpleSystem_0_mainBlock_pA1outState_6Output_AgentOutputConstraint_OutputDef_Output_writeByWeight.connect(new ArrayList<Case>(Arrays.asList(
			new Case(simpleSystem_0_mainBlock_pA1outState_6Output_AgentOutputConstraint_OutputDef_Output_writeByWeight_FilterByDef_FilterBy_death, simpleSystem_0_mainBlock_pA1outState_6Output_AgentOutputConstraint_OutputDef_Output_writeByWeight_FilterByDef_86),
			new Case(simpleSystem_0_mainBlock_pA1outState_6Output_AgentOutputConstraint_OutputDef_Output_writeByWeight_FilterByDef_FilterBy_concatWait, simpleSystem_0_mainBlock_pA1outState_6Output_AgentOutputConstraint_OutputDef_Output_writeByWeight_FilterByDef_78))));
		simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_readOneThenByWeightPlusOne_FilterByDef_FilterBy_FilterBySeqTail.connect(new ArrayList<Case>(Arrays.asList()));
		simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_readByWeight_FilterByDef_FilterBy_FilterBySeqTail.connect(new ArrayList<Case>(Arrays.asList()));
		simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredByExecTime.connect(new ArrayList<Case>(Arrays.asList(
			new Case(simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredByExecTime_FilterByDef_FilterBy_death, simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredByExecTime_FilterByDef_81),
			new Case(simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredByExecTime_FilterByDef_FilterBy_concatWait, simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredByExecTime_FilterByDef_94))));
		simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_pushByWeightPlusOne.connect(new ArrayList<Case>(Arrays.asList(
			new Case(simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_pushByWeightPlusOne_FilterByDef_FilterBy_death, simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_pushByWeightPlusOne_FilterByDef_75),
			new Case(simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_pushByWeightPlusOne_FilterByDef_FilterBy_concatWait, simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_pushByWeightPlusOne_FilterByDef_76))));
		simpleSystem_0_mainBlock_place1_7_DelayFor_NwriteInAdvance.connect(simpleSystem_0_mainBlock_place1_7_DelayFor_NwriteInAdvance_DelayForDef_DelayFor_defer);
		simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredByExecTime.connect(new ArrayList<Case>(Arrays.asList(
			new Case(simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredByExecTime_FilterByDef_FilterBy_death, simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredByExecTime_FilterByDef_52),
			new Case(simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredByExecTime_FilterByDef_FilterBy_concatWait, simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredByExecTime_FilterByDef_84))));
		simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_readByWeightPlusOne_FilterByDef_FilterBy_FilterBySeqTail.connect(new ArrayList<Case>(Arrays.asList()));
		simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_readOneThenByWeightPlusOne.connect(new ArrayList<Case>(Arrays.asList(
			new Case(simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_readOneThenByWeightPlusOne_FilterByDef_FilterBy_death, simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_readOneThenByWeightPlusOne_FilterByDef_69),
			new Case(simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_readOneThenByWeightPlusOne_FilterByDef_FilterBy_concatWait, simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_readOneThenByWeightPlusOne_FilterByDef_98))));
		simpleSystem_0_mainBlock_pA1out_5Output_AgentOutputConstraint_OutputDef_Output_writeByWeight.connect(new ArrayList<Case>(Arrays.asList(
			new Case(simpleSystem_0_mainBlock_pA1out_5Output_AgentOutputConstraint_OutputDef_Output_writeByWeight_FilterByDef_FilterBy_death, simpleSystem_0_mainBlock_pA1out_5Output_AgentOutputConstraint_OutputDef_Output_writeByWeight_FilterByDef_65),
			new Case(simpleSystem_0_mainBlock_pA1out_5Output_AgentOutputConstraint_OutputDef_Output_writeByWeight_FilterByDef_FilterBy_concatWait, simpleSystem_0_mainBlock_pA1out_5Output_AgentOutputConstraint_OutputDef_Output_writeByWeight_FilterByDef_90))));
		simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredStartThenByExecTime_FilterByDef_FilterBy_FilterBySeqTail.connect(new ArrayList<Case>(Arrays.asList()));
		simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredStartThenByExecTime_FilterByDef_FilterBy_FilterBySeqTail.connect(new ArrayList<Case>(Arrays.asList()));
		simpleSystem_0_mainBlock_pA1outState_6Output_AgentOutputConstraint_OutputDef_Output_writeByWeight_FilterByDef_FilterBy_FilterBySeqTail.connect(new ArrayList<Case>(Arrays.asList()));
		simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_pushByWeightPlusOne_FilterByDef_FilterBy_FilterBySeqTail.connect(new ArrayList<Case>(Arrays.asList()));
		simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredStartThenByExecTime.connect(new ArrayList<Case>(Arrays.asList(
			new Case(simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredStartThenByExecTime_FilterByDef_FilterBy_death, simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredStartThenByExecTime_FilterByDef_80),
			new Case(simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredStartThenByExecTime_FilterByDef_FilterBy_concatWait, simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredStartThenByExecTime_FilterByDef_61))));
		simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_readByWeightPlusOne_FilterByDef_FilterBy_FilterBySeqTail.connect(new ArrayList<Case>(Arrays.asList()));
		simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_readByWeight.connect(new ArrayList<Case>(Arrays.asList(
			new Case(simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_readByWeight_FilterByDef_FilterBy_death, simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_readByWeight_FilterByDef_99),
			new Case(simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_readByWeight_FilterByDef_FilterBy_concatWait, simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_readByWeight_FilterByDef_88))));
		simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_readByWeightPlusOne.connect(new ArrayList<Case>(Arrays.asList(
			new Case(simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_readByWeightPlusOne_FilterByDef_FilterBy_death, simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_readByWeightPlusOne_FilterByDef_83),
			new Case(simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_readByWeightPlusOne_FilterByDef_FilterBy_concatWait, simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_readByWeightPlusOne_FilterByDef_91))));
		simpleSystem_0_mainBlock_place1_7Token_TokenConstraint.connect(simpleSystem_0_mainBlock_place1_7Token_TokenConstraint_TokenDef_Token_causalityImmediate1);		simpleSystem_0_mainBlock_placeA1State_8Token_TokenConstraint.connect(simpleSystem_0_mainBlock_placeA1State_8Token_TokenConstraint_TokenDef_Token_causalityImmediate2);	Set<Constraint> allExclusions = new HashSet<Constraint>(Arrays.asList(simpleSystem_0_mainBlock_mem1_10Exclusion_IsReadingXorWriting,simpleSystem_0_mainBlock_CPU1_9intermediate_oneAgentAtATime1_2,simpleSystem_0_mainBlock_CPU1_9Exclusion_IsExecutingXorIdle));
	Set<Constraint> allConstraintsButExclusions = new HashSet<Constraint>(Arrays.asList(simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_stopAfterExecTime,simpleSystem_0_mainBlock_A1_2Alternates_NonReentrantAgent_AlternatesDef_Alt_c2precedesC1DelayedByOne,simpleSystem_0_mainBlock_pA1out_5Coincides_portWriteInPlace,simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_weightTokenCausesExec,simpleSystem_0_mainBlock_placeA1State_8Token_TokenConstraint_TokenDef_Token_causalityImmediate1,simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_weightPlusOneTokenCausesExec,simpleSystem_0_mainBlock_pA1outState_6Coincides_portWriteInPlace,simpleSystem_0_mainBlock_placeA1State_8Precedes_BoundedPlaces,simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_weightPlusOneTokenCausesExec,simpleSystem_0_mainBlock_A1_2Alternates_NonReentrantAgent_AlternatesDef_Alt_c1PrecedesC2,simpleSystem_0_mainBlock_placeA1State_8Token_TokenConstraint_TokenDef_Token_causalityDelayed,simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_stopAfterExecTime2,simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_stopAfterExecTime,simpleSystem_0_mainBlock_placeA1State_8Token_TokenConstraint_TokenDef_Token_causalityImmediate2,simpleSystem_0_mainBlock_pA1outState_6Output_AgentOutputConstraint_OutputDef_Output_ExecWriteFirstToken,simpleSystem_0_mainBlock_pA2in_4Coincides_portReadInPlace,simpleSystem_0_mainBlock_CPU1_9Alternates_NonPreemptiveExecution_AlternatesDef_Alt_c2precedesC1DelayedByOne,simpleSystem_0_mainBlock_A2_1Alternates_NonReentrantAgent_AlternatesDef_Alt_c2precedesC1DelayedByOne,simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_weightTokenCausesExec,simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_weightTokenCausesExec,simpleSystem_0_mainBlock_place1_7Precedes_BoundedPlaces,simpleSystem_0_mainBlock_place1_7Token_TokenConstraint_TokenDef_Token_causalityDelayed,simpleSystem_0_mainBlock_A2_1Alternates_NonReentrantAgent_AlternatesDef_Alt_c1PrecedesC2,simpleSystem_0_mainBlock_CPU1_9Coincides_ExecutesItsAllocatedAgents,simpleSystem_0_mainBlock_CPU1_9Alternates_NonPreemptiveExecution_AlternatesDef_Alt_c1PrecedesC2,simpleSystem_0_mainBlock_pA1out_5Output_AgentOutputConstraint_OutputDef_Output_ExecWriteFirstToken,simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_stopAfterExecTime2,simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_weightTokenCausesExec,simpleSystem_0_mainBlock_mem1_10Coincides_isWritingAccordingToPortWrite,simpleSystem_0_mainBlock_mem1_10Coincides_isReadingAccordingToPortRead,simpleSystem_0_mainBlock_place1_7Token_TokenConstraint_TokenDef_Token_causalityImmediate2,simpleSystem_0_mainBlock_place1_7Token_TokenConstraint_TokenDef_Token_causalityImmediate1,simpleSystem_0_mainBlock_pA1inState_3Coincides_portReadInPlace,simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_pushByWeightPlusOne_FilterByDef_killIt,simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_readByWeight_FilterByDef_FilterBy_concatWait,simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_readOneThenByWeightPlusOne_FilterByDef_killIt,simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_readByWeight_FilterByDef_killIt,simpleSystem_0_mainBlock_pA1outState_6Output_AgentOutputConstraint_OutputDef_Output_writeByWeight_FilterByDef_FilterBy_concatWait,simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredByExecTime_FilterByDef_FilterBy_waitHead,simpleSystem_0_mainBlock_placeA1State_8_DelayFor_NwriteInAdvance_DelayForDef_DelayFor_defer,simpleSystem_0_mainBlock_pA1out_5Output_AgentOutputConstraint_OutputDef_Output_writeByWeight_FilterByDef_FilterBy_concatWait,simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_readByWeight_FilterByDef_FilterBy_waitHead,simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredByExecTime_FilterByDef_FilterBy_concatWait,simpleSystem_0_mainBlock_pA1outState_6Output_AgentOutputConstraint_OutputDef_Output_writeByWeight_FilterByDef_killIt,simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_pushByWeightPlusOne_FilterByDef_FilterBy_concatWait,simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_pushByWeightPlusOne_FilterByDef_FilterBy_death,simpleSystem_0_mainBlock_CPU1_9intermediate_allAgentStops1,simpleSystem_0_mainBlock_A1_2Alternates_NonReentrantAgent_AlternatesDef_Alt_c1DelayedByOne,simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredByExecTime_FilterByDef_killIt,simpleSystem_0_mainBlock_pA1out_5Output_AgentOutputConstraint_OutputDef_Output_writeByWeight_FilterByDef_killIt,simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_readByWeightPlusOne_FilterByDef_killIt,simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_readByWeightPlusOne_FilterByDef_FilterBy_concatWait,simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredByExecTime_FilterByDef_killIt,simpleSystem_0_mainBlock_A2_1Alternates_NonReentrantAgent_AlternatesDef_Alt_c1DelayedByOne,simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_readByWeightPlusOne_FilterByDef_FilterBy_waitHead,simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredStartThenByExecTime_FilterByDef_killIt,simpleSystem_0_mainBlock_CPU1_9intermediate_allAgentsExecutions1,simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_pushByWeightPlusOne_FilterByDef_killIt,simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_readOneThenByWeightPlusOne_FilterByDef_FilterBy_death,simpleSystem_0_mainBlock_placeA1State_8Token_TokenConstraint_TokenDef_Token_readDelayedForDelay,simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_readByWeightPlusOne_FilterByDef_FilterBy_concatWait,simpleSystem_0_mainBlock_pA1out_5Output_AgentOutputConstraint_OutputDef_Output_writeByWeight_FilterByDef_FilterBy_death,simpleSystem_0_mainBlock_mem1_10intermediate_allPortWrites1,simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredStartThenByExecTime_FilterByDef_FilterBy_death,simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_readOneThenByWeightPlusOne_FilterByDef_killIt,simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_readOneThenByWeightPlusOne_FilterByDef_FilterBy_death,simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_readByWeight_FilterByDef_FilterBy_death,simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredStartThenByExecTime_FilterByDef_FilterBy_concatWait,simpleSystem_0_mainBlock_pA1out_5Output_AgentOutputConstraint_OutputDef_Output_writeByWeight_FilterByDef_FilterBy_waitHead,simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredByExecTime_FilterByDef_FilterBy_waitHead,simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_pushByWeightPlusOne_FilterByDef_FilterBy_waitHead,simpleSystem_0_mainBlock_place1_7_DelayFor_NwriteInAdvance_DelayForDef_DelayFor_defer,simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_readOneThenByWeightPlusOne_FilterByDef_FilterBy_concatWait,simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_readByWeightPlusOne_FilterByDef_FilterBy_waitHead,simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredStartThenByExecTime_FilterByDef_killIt,simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredStartThenByExecTime_FilterByDef_FilterBy_waitHead,simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_readByWeight_FilterByDef_killIt,simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_pushByWeightPlusOne_FilterByDef_FilterBy_concatWait,simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_readOneThenByWeightPlusOne_FilterByDef_FilterBy_concatWait,simpleSystem_0_mainBlock_mem1_10intermediate_allPortReads1,simpleSystem_0_mainBlock_pA1outState_6Output_AgentOutputConstraint_OutputDef_Output_writeByWeight_FilterByDef_FilterBy_waitHead,simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredByExecTime_FilterByDef_FilterBy_death,simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_readByWeight_FilterByDef_FilterBy_waitHead,simpleSystem_0_mainBlock_CPU1_9Alternates_NonPreemptiveExecution_AlternatesDef_Alt_c1DelayedByOne,simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredByExecTime_FilterByDef_FilterBy_death,simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredStartThenByExecTime_FilterByDef_FilterBy_waitHead,simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_readOneThenByWeightPlusOne_FilterByDef_FilterBy_waitHead,simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_pushByWeightPlusOne_FilterByDef_FilterBy_waitHead,simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_pushByWeightPlusOne_FilterByDef_FilterBy_death,simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_readByWeight_FilterByDef_FilterBy_death,simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredStartThenByExecTime_FilterByDef_FilterBy_concatWait,simpleSystem_0_mainBlock_place1_7Token_TokenConstraint_TokenDef_Token_readDelayedForDelay,simpleSystem_0_mainBlock_pA1outState_6Output_AgentOutputConstraint_OutputDef_Output_writeByWeight_FilterByDef_FilterBy_death,simpleSystem_0_mainBlock_CPU1_9intermediate_allAgentStarts1,simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredByExecTime_FilterByDef_FilterBy_concatWait,simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_readByWeightPlusOne_FilterByDef_FilterBy_death,simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_readByWeight_FilterByDef_FilterBy_concatWait,simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_readOneThenByWeightPlusOne_FilterByDef_FilterBy_waitHead,simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_readByWeightPlusOne_FilterByDef_killIt,simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_readByWeightPlusOne_FilterByDef_FilterBy_death,simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredStartThenByExecTime_FilterByDef_FilterBy_death,simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_readByWeight_FilterByDef_FilterBy_concatWait,simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_readByWeight_FilterByDef_killIt,simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_readByWeight_FilterByDef_FilterBy_FilterBySeqTail,simpleSystem_0_mainBlock_pA1outState_6Output_AgentOutputConstraint_OutputDef_Output_writeByWeight_FilterByDef_FilterBy_concatWait,simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredByExecTime_FilterByDef_FilterBy_waitHead,simpleSystem_0_mainBlock_placeA1State_8_DelayFor_NwriteInAdvance_DelayForDef_DelayFor_defer,simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_readByWeightPlusOne,simpleSystem_0_mainBlock_pA1out_5Output_AgentOutputConstraint_OutputDef_Output_writeByWeight_FilterByDef_FilterBy_concatWait,simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredByExecTime_FilterByDef_FilterBy_concatWait,simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_readOneThenByWeightPlusOne,simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_pushByWeightPlusOne_FilterByDef_FilterBy_death,simpleSystem_0_mainBlock_CPU1_9intermediate_allAgentStops1,simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredByExecTime_FilterByDef_killIt,simpleSystem_0_mainBlock_pA1out_5Output_AgentOutputConstraint_OutputDef_Output_writeByWeight_FilterByDef_killIt,simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_readByWeightPlusOne_FilterByDef_FilterBy_concatWait,simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredStartThenByExecTime,simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredByExecTime_FilterByDef_killIt,simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_pushByWeightPlusOne,simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_readByWeight,simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_readByWeightPlusOne_FilterByDef_FilterBy_waitHead,simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredByExecTime_FilterByDef_FilterBy_FilterBySeqTail,simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_pushByWeightPlusOne_FilterByDef_killIt,simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_readOneThenByWeightPlusOne_FilterByDef_FilterBy_FilterBySeqTail,simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_readOneThenByWeightPlusOne_FilterByDef_FilterBy_death,simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_pushByWeightPlusOne_FilterByDef_FilterBy_FilterBySeqTail,simpleSystem_0_mainBlock_placeA1State_8Token_TokenConstraint_TokenDef_Token_readDelayedForDelay,simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_readByWeightPlusOne_FilterByDef_FilterBy_concatWait,simpleSystem_0_mainBlock_pA1out_5Output_AgentOutputConstraint_OutputDef_Output_writeByWeight_FilterByDef_FilterBy_death,simpleSystem_0_mainBlock_mem1_10intermediate_allPortWrites1,simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_readOneThenByWeightPlusOne_FilterByDef_FilterBy_death,simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_readByWeight_FilterByDef_FilterBy_death,simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredStartThenByExecTime_FilterByDef_FilterBy_concatWait,simpleSystem_0_mainBlock_pA1out_5Output_AgentOutputConstraint_OutputDef_Output_writeByWeight_FilterByDef_FilterBy_waitHead,simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_pushByWeightPlusOne_FilterByDef_FilterBy_waitHead,simpleSystem_0_mainBlock_placeA1State_8_DelayFor_NwriteInAdvance,simpleSystem_0_mainBlock_place1_7_DelayFor_NwriteInAdvance_DelayForDef_DelayFor_defer,simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_readOneThenByWeightPlusOne_FilterByDef_FilterBy_concatWait,simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_readByWeightPlusOne_FilterByDef_FilterBy_waitHead,simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredStartThenByExecTime_FilterByDef_killIt,simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_readOneThenByWeightPlusOne_FilterByDef_FilterBy_concatWait,simpleSystem_0_mainBlock_mem1_10intermediate_allPortReads1,simpleSystem_0_mainBlock_pA1outState_6Output_AgentOutputConstraint_OutputDef_Output_writeByWeight_FilterByDef_FilterBy_waitHead,simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredByExecTime_FilterByDef_FilterBy_FilterBySeqTail,simpleSystem_0_mainBlock_CPU1_9Alternates_NonPreemptiveExecution_AlternatesDef_Alt_c1DelayedByOne,simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_pushByWeightPlusOne_FilterByDef_FilterBy_waitHead,simpleSystem_0_mainBlock_pA1out_5Output_AgentOutputConstraint_OutputDef_Output_writeByWeight_FilterByDef_FilterBy_FilterBySeqTail,simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredStartThenByExecTime_FilterByDef_FilterBy_concatWait,simpleSystem_0_mainBlock_place1_7Token_TokenConstraint_TokenDef_Token_readDelayedForDelay,simpleSystem_0_mainBlock_CPU1_9intermediate_allAgentStarts1,simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredByExecTime_FilterByDef_FilterBy_concatWait,simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_readByWeightPlusOne_FilterByDef_FilterBy_death,simpleSystem_0_mainBlock_pA1outState_6Output_AgentOutputConstraint_OutputDef_Output_writeByWeight,simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_readByWeightPlusOne_FilterByDef_killIt,simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_readOneThenByWeightPlusOne_FilterByDef_FilterBy_FilterBySeqTail,simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_readByWeight_FilterByDef_FilterBy_FilterBySeqTail,simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_readByWeightPlusOne_FilterByDef_FilterBy_death,simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredStartThenByExecTime_FilterByDef_FilterBy_death,simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_pushByWeightPlusOne_FilterByDef_killIt,simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_readOneThenByWeightPlusOne_FilterByDef_killIt,simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredByExecTime,simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_readByWeight_FilterByDef_FilterBy_waitHead,simpleSystem_0_mainBlock_pA1outState_6Output_AgentOutputConstraint_OutputDef_Output_writeByWeight_FilterByDef_killIt,simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_pushByWeightPlusOne_FilterByDef_FilterBy_concatWait,simpleSystem_0_mainBlock_A1_2Alternates_NonReentrantAgent_AlternatesDef_Alt_c1DelayedByOne,simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_pushByWeightPlusOne,simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_readByWeightPlusOne_FilterByDef_killIt,simpleSystem_0_mainBlock_A2_1Alternates_NonReentrantAgent_AlternatesDef_Alt_c1DelayedByOne,simpleSystem_0_mainBlock_place1_7_DelayFor_NwriteInAdvance,simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredStartThenByExecTime_FilterByDef_killIt,simpleSystem_0_mainBlock_CPU1_9intermediate_allAgentsExecutions1,simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredByExecTime,simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredStartThenByExecTime_FilterByDef_FilterBy_death,simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_readOneThenByWeightPlusOne_FilterByDef_killIt,simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_readByWeightPlusOne_FilterByDef_FilterBy_FilterBySeqTail,simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_readOneThenByWeightPlusOne,simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredByExecTime_FilterByDef_FilterBy_waitHead,simpleSystem_0_mainBlock_pA1out_5Output_AgentOutputConstraint_OutputDef_Output_writeByWeight,simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredStartThenByExecTime_FilterByDef_FilterBy_FilterBySeqTail,simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredStartThenByExecTime_FilterByDef_FilterBy_waitHead,simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_readByWeight_FilterByDef_killIt,simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_pushByWeightPlusOne_FilterByDef_FilterBy_concatWait,simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredStartThenByExecTime_FilterByDef_FilterBy_FilterBySeqTail,simpleSystem_0_mainBlock_pA1outState_6Output_AgentOutputConstraint_OutputDef_Output_writeByWeight_FilterByDef_FilterBy_FilterBySeqTail,simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredByExecTime_FilterByDef_FilterBy_death,simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_readByWeight_FilterByDef_FilterBy_waitHead,simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredByExecTime_FilterByDef_FilterBy_death,simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_pushByWeightPlusOne_FilterByDef_FilterBy_FilterBySeqTail,simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredStartThenByExecTime_FilterByDef_FilterBy_waitHead,simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_readOneThenByWeightPlusOne_FilterByDef_FilterBy_waitHead,simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_pushByWeightPlusOne_FilterByDef_FilterBy_death,simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_readByWeight_FilterByDef_FilterBy_death,simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredStartThenByExecTime,simpleSystem_0_mainBlock_pA1outState_6Output_AgentOutputConstraint_OutputDef_Output_writeByWeight_FilterByDef_FilterBy_death,simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_readByWeightPlusOne_FilterByDef_FilterBy_FilterBySeqTail,simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_readByWeight,simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_readByWeight_FilterByDef_FilterBy_concatWait,simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_readByWeightPlusOne,simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_readOneThenByWeightPlusOne_FilterByDef_FilterBy_waitHead));
	Set<Clock> allClocks = new HashSet<Clock>(Arrays.asList(simpleSystem_0_mainBlock_CPU1_9_isExecuting,simpleSystem_0_mainBlock_pA1inState_3_read,simpleSystem_0_mainBlock_A2_1_startAgent,simpleSystem_0_mainBlock_mem1_10_isWriting,simpleSystem_0_mainBlock_placeA1State_8_pop,simpleSystem_0_mainBlock_A1_2_stopAgent,simpleSystem_0_mainBlock_place1_7_pop,simpleSystem_0_mainBlock_pA1outState_6_write,simpleSystem_0_mainBlock_placeA1State_8_push,simpleSystem_0_mainBlock_CPU1_9_idle,simpleSystem_0_mainBlock_mem1_10_isReading,simpleSystem_0_mainBlock_A1_2_isExecuting,simpleSystem_0_mainBlock_A2_1_stopAgent,simpleSystem_0_mainBlock_A2_1_isExecuting,simpleSystem_0_mainBlock_place1_7_push,simpleSystem_0_mainBlock_A1_2_startAgent,simpleSystem_0_mainBlock_pA2in_4_read,simpleSystem_0_mainBlock_pA1out_5_write,simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_pushByWeightPlusOne_FilterByDef_killIt,simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_readByWeight_FilterByDef_FilterBy_concatWait,simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_readOneThenByWeightPlusOne_FilterByDef_killIt,simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_readByWeight_FilterByDef_killIt,simpleSystem_0_mainBlock_pA1outState_6Output_AgentOutputConstraint_OutputDef_Output_writeByWeight_FilterByDef_FilterBy_concatWait,simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredByExecTime_FilterByDef_FilterBy_waitHead,simpleSystem_0_mainBlock_placeA1State_8_DelayFor_NwriteInAdvance_DelayForDef_DelayFor_defer,simpleSystem_0_mainBlock_pA1out_5Output_AgentOutputConstraint_OutputDef_Output_writeByWeight_FilterByDef_FilterBy_concatWait,simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_readByWeight_FilterByDef_FilterBy_waitHead,simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredByExecTime_FilterByDef_FilterBy_concatWait,simpleSystem_0_mainBlock_pA1outState_6Output_AgentOutputConstraint_OutputDef_Output_writeByWeight_FilterByDef_killIt,simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_pushByWeightPlusOne_FilterByDef_FilterBy_concatWait,simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_pushByWeightPlusOne_FilterByDef_FilterBy_death,simpleSystem_0_mainBlock_CPU1_9intermediate_allAgentStops1,simpleSystem_0_mainBlock_A1_2Alternates_NonReentrantAgent_AlternatesDef_Alt_c1DelayedByOne,simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredByExecTime_FilterByDef_killIt,simpleSystem_0_mainBlock_pA1out_5Output_AgentOutputConstraint_OutputDef_Output_writeByWeight_FilterByDef_killIt,simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_readByWeightPlusOne_FilterByDef_killIt,simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_readByWeightPlusOne_FilterByDef_FilterBy_concatWait,simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredByExecTime_FilterByDef_killIt,simpleSystem_0_mainBlock_A2_1Alternates_NonReentrantAgent_AlternatesDef_Alt_c1DelayedByOne,simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_readByWeightPlusOne_FilterByDef_FilterBy_waitHead,simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredStartThenByExecTime_FilterByDef_killIt,simpleSystem_0_mainBlock_CPU1_9intermediate_allAgentsExecutions1,simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_pushByWeightPlusOne_FilterByDef_killIt,simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_readOneThenByWeightPlusOne_FilterByDef_FilterBy_death,simpleSystem_0_mainBlock_placeA1State_8Token_TokenConstraint_TokenDef_Token_readDelayedForDelay,simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_readByWeightPlusOne_FilterByDef_FilterBy_concatWait,simpleSystem_0_mainBlock_pA1out_5Output_AgentOutputConstraint_OutputDef_Output_writeByWeight_FilterByDef_FilterBy_death,simpleSystem_0_mainBlock_mem1_10intermediate_allPortWrites1,simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredStartThenByExecTime_FilterByDef_FilterBy_death,simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_readOneThenByWeightPlusOne_FilterByDef_killIt,simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_readOneThenByWeightPlusOne_FilterByDef_FilterBy_death,simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_readByWeight_FilterByDef_FilterBy_death,simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredStartThenByExecTime_FilterByDef_FilterBy_concatWait,simpleSystem_0_mainBlock_pA1out_5Output_AgentOutputConstraint_OutputDef_Output_writeByWeight_FilterByDef_FilterBy_waitHead,simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredByExecTime_FilterByDef_FilterBy_waitHead,simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_pushByWeightPlusOne_FilterByDef_FilterBy_waitHead,simpleSystem_0_mainBlock_place1_7_DelayFor_NwriteInAdvance_DelayForDef_DelayFor_defer,simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_readOneThenByWeightPlusOne_FilterByDef_FilterBy_concatWait,simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_readByWeightPlusOne_FilterByDef_FilterBy_waitHead,simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredStartThenByExecTime_FilterByDef_killIt,simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredStartThenByExecTime_FilterByDef_FilterBy_waitHead,simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_readByWeight_FilterByDef_killIt,simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_pushByWeightPlusOne_FilterByDef_FilterBy_concatWait,simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_readOneThenByWeightPlusOne_FilterByDef_FilterBy_concatWait,simpleSystem_0_mainBlock_mem1_10intermediate_allPortReads1,simpleSystem_0_mainBlock_pA1outState_6Output_AgentOutputConstraint_OutputDef_Output_writeByWeight_FilterByDef_FilterBy_waitHead,simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredByExecTime_FilterByDef_FilterBy_death,simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_readByWeight_FilterByDef_FilterBy_waitHead,simpleSystem_0_mainBlock_CPU1_9Alternates_NonPreemptiveExecution_AlternatesDef_Alt_c1DelayedByOne,simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredByExecTime_FilterByDef_FilterBy_death,simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredStartThenByExecTime_FilterByDef_FilterBy_waitHead,simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_readOneThenByWeightPlusOne_FilterByDef_FilterBy_waitHead,simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_pushByWeightPlusOne_FilterByDef_FilterBy_waitHead,simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_pushByWeightPlusOne_FilterByDef_FilterBy_death,simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_readByWeight_FilterByDef_FilterBy_death,simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredStartThenByExecTime_FilterByDef_FilterBy_concatWait,simpleSystem_0_mainBlock_place1_7Token_TokenConstraint_TokenDef_Token_readDelayedForDelay,simpleSystem_0_mainBlock_pA1outState_6Output_AgentOutputConstraint_OutputDef_Output_writeByWeight_FilterByDef_FilterBy_death,simpleSystem_0_mainBlock_CPU1_9intermediate_allAgentStarts1,simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredByExecTime_FilterByDef_FilterBy_concatWait,simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_readByWeightPlusOne_FilterByDef_FilterBy_death,simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_readByWeight_FilterByDef_FilterBy_concatWait,simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_readOneThenByWeightPlusOne_FilterByDef_FilterBy_waitHead,simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_readByWeightPlusOne_FilterByDef_killIt,simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_readByWeightPlusOne_FilterByDef_FilterBy_death,simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredStartThenByExecTime_FilterByDef_FilterBy_death,simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_readByWeight_FilterByDef_FilterBy_concatWait,simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_readByWeight_FilterByDef_killIt,simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_readByWeight_FilterByDef_FilterBy_FilterBySeqTail,simpleSystem_0_mainBlock_pA1outState_6Output_AgentOutputConstraint_OutputDef_Output_writeByWeight_FilterByDef_FilterBy_concatWait,simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredByExecTime_FilterByDef_FilterBy_waitHead,simpleSystem_0_mainBlock_placeA1State_8_DelayFor_NwriteInAdvance_DelayForDef_DelayFor_defer,simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_readByWeightPlusOne,simpleSystem_0_mainBlock_pA1out_5Output_AgentOutputConstraint_OutputDef_Output_writeByWeight_FilterByDef_FilterBy_concatWait,simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredByExecTime_FilterByDef_FilterBy_concatWait,simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_readOneThenByWeightPlusOne,simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_pushByWeightPlusOne_FilterByDef_FilterBy_death,simpleSystem_0_mainBlock_CPU1_9intermediate_allAgentStops1,simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredByExecTime_FilterByDef_killIt,simpleSystem_0_mainBlock_pA1out_5Output_AgentOutputConstraint_OutputDef_Output_writeByWeight_FilterByDef_killIt,simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_readByWeightPlusOne_FilterByDef_FilterBy_concatWait,simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredStartThenByExecTime,simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredByExecTime_FilterByDef_killIt,simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_pushByWeightPlusOne,simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_readByWeight,simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_readByWeightPlusOne_FilterByDef_FilterBy_waitHead,simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredByExecTime_FilterByDef_FilterBy_FilterBySeqTail,simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_pushByWeightPlusOne_FilterByDef_killIt,simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_readOneThenByWeightPlusOne_FilterByDef_FilterBy_FilterBySeqTail,simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_readOneThenByWeightPlusOne_FilterByDef_FilterBy_death,simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_pushByWeightPlusOne_FilterByDef_FilterBy_FilterBySeqTail,simpleSystem_0_mainBlock_placeA1State_8Token_TokenConstraint_TokenDef_Token_readDelayedForDelay,simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_readByWeightPlusOne_FilterByDef_FilterBy_concatWait,simpleSystem_0_mainBlock_pA1out_5Output_AgentOutputConstraint_OutputDef_Output_writeByWeight_FilterByDef_FilterBy_death,simpleSystem_0_mainBlock_mem1_10intermediate_allPortWrites1,simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_readOneThenByWeightPlusOne_FilterByDef_FilterBy_death,simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_readByWeight_FilterByDef_FilterBy_death,simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredStartThenByExecTime_FilterByDef_FilterBy_concatWait,simpleSystem_0_mainBlock_pA1out_5Output_AgentOutputConstraint_OutputDef_Output_writeByWeight_FilterByDef_FilterBy_waitHead,simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_pushByWeightPlusOne_FilterByDef_FilterBy_waitHead,simpleSystem_0_mainBlock_placeA1State_8_DelayFor_NwriteInAdvance,simpleSystem_0_mainBlock_place1_7_DelayFor_NwriteInAdvance_DelayForDef_DelayFor_defer,simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_readOneThenByWeightPlusOne_FilterByDef_FilterBy_concatWait,simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_readByWeightPlusOne_FilterByDef_FilterBy_waitHead,simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredStartThenByExecTime_FilterByDef_killIt,simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_readOneThenByWeightPlusOne_FilterByDef_FilterBy_concatWait,simpleSystem_0_mainBlock_mem1_10intermediate_allPortReads1,simpleSystem_0_mainBlock_pA1outState_6Output_AgentOutputConstraint_OutputDef_Output_writeByWeight_FilterByDef_FilterBy_waitHead,simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredByExecTime_FilterByDef_FilterBy_FilterBySeqTail,simpleSystem_0_mainBlock_CPU1_9Alternates_NonPreemptiveExecution_AlternatesDef_Alt_c1DelayedByOne,simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_pushByWeightPlusOne_FilterByDef_FilterBy_waitHead,simpleSystem_0_mainBlock_pA1out_5Output_AgentOutputConstraint_OutputDef_Output_writeByWeight_FilterByDef_FilterBy_FilterBySeqTail,simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredStartThenByExecTime_FilterByDef_FilterBy_concatWait,simpleSystem_0_mainBlock_place1_7Token_TokenConstraint_TokenDef_Token_readDelayedForDelay,simpleSystem_0_mainBlock_CPU1_9intermediate_allAgentStarts1,simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredByExecTime_FilterByDef_FilterBy_concatWait,simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_readByWeightPlusOne_FilterByDef_FilterBy_death,simpleSystem_0_mainBlock_pA1outState_6Output_AgentOutputConstraint_OutputDef_Output_writeByWeight,simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_readByWeightPlusOne_FilterByDef_killIt,simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_readOneThenByWeightPlusOne_FilterByDef_FilterBy_FilterBySeqTail,simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_readByWeight_FilterByDef_FilterBy_FilterBySeqTail,simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_readByWeightPlusOne_FilterByDef_FilterBy_death,simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredStartThenByExecTime_FilterByDef_FilterBy_death,simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_pushByWeightPlusOne_FilterByDef_killIt,simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_readOneThenByWeightPlusOne_FilterByDef_killIt,simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredByExecTime,simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_readByWeight_FilterByDef_FilterBy_waitHead,simpleSystem_0_mainBlock_pA1outState_6Output_AgentOutputConstraint_OutputDef_Output_writeByWeight_FilterByDef_killIt,simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_pushByWeightPlusOne_FilterByDef_FilterBy_concatWait,simpleSystem_0_mainBlock_A1_2Alternates_NonReentrantAgent_AlternatesDef_Alt_c1DelayedByOne,simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_pushByWeightPlusOne,simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_readByWeightPlusOne_FilterByDef_killIt,simpleSystem_0_mainBlock_A2_1Alternates_NonReentrantAgent_AlternatesDef_Alt_c1DelayedByOne,simpleSystem_0_mainBlock_place1_7_DelayFor_NwriteInAdvance,simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredStartThenByExecTime_FilterByDef_killIt,simpleSystem_0_mainBlock_CPU1_9intermediate_allAgentsExecutions1,simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredByExecTime,simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredStartThenByExecTime_FilterByDef_FilterBy_death,simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_readOneThenByWeightPlusOne_FilterByDef_killIt,simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_readByWeightPlusOne_FilterByDef_FilterBy_FilterBySeqTail,simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_readOneThenByWeightPlusOne,simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredByExecTime_FilterByDef_FilterBy_waitHead,simpleSystem_0_mainBlock_pA1out_5Output_AgentOutputConstraint_OutputDef_Output_writeByWeight,simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredStartThenByExecTime_FilterByDef_FilterBy_FilterBySeqTail,simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredStartThenByExecTime_FilterByDef_FilterBy_waitHead,simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_readByWeight_FilterByDef_killIt,simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_pushByWeightPlusOne_FilterByDef_FilterBy_concatWait,simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredStartThenByExecTime_FilterByDef_FilterBy_FilterBySeqTail,simpleSystem_0_mainBlock_pA1outState_6Output_AgentOutputConstraint_OutputDef_Output_writeByWeight_FilterByDef_FilterBy_FilterBySeqTail,simpleSystem_0_mainBlock_A2_1AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredByExecTime_FilterByDef_FilterBy_death,simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_readByWeight_FilterByDef_FilterBy_waitHead,simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredByExecTime_FilterByDef_FilterBy_death,simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_pushByWeightPlusOne_FilterByDef_FilterBy_FilterBySeqTail,simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredStartThenByExecTime_FilterByDef_FilterBy_waitHead,simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_readOneThenByWeightPlusOne_FilterByDef_FilterBy_waitHead,simpleSystem_0_mainBlock_place1_7PlaceRate_waitEnoughData_PlaceRateDef_Place_pushByWeightPlusOne_FilterByDef_FilterBy_death,simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_readByWeight_FilterByDef_FilterBy_death,simpleSystem_0_mainBlock_A1_2AgentExecution_ComputationDelay_AgentExecutionDef_AgentExecution_refFilteredStartThenByExecTime,simpleSystem_0_mainBlock_pA1outState_6Output_AgentOutputConstraint_OutputDef_Output_writeByWeight_FilterByDef_FilterBy_death,simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_readByWeightPlusOne_FilterByDef_FilterBy_FilterBySeqTail,simpleSystem_0_mainBlock_pA1inState_3Input_AgentInputConstraint_InputDef_Input_readByWeight,simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_readByWeight_FilterByDef_FilterBy_concatWait,simpleSystem_0_mainBlock_pA2in_4Input_AgentInputConstraint_InputDef_Input_readByWeightPlusOne,simpleSystem_0_mainBlock_placeA1State_8PlaceRate_waitEnoughData_PlaceRateDef_Place_readOneThenByWeightPlusOne_FilterByDef_FilterBy_waitHead));


theEngine.clockToEObject.put(simpleSystem_0_mainBlock_A2_1_startAgent, "//@ownedApplication/@ownedAgents.1");
theEngine.clockToMethodName.put(simpleSystem_0_mainBlock_A2_1_startAgent, "execute");
theEngine.clockToEObject.put(simpleSystem_0_mainBlock_A1_2_startAgent, "//@ownedApplication/@ownedAgents.0");
theEngine.clockToMethodName.put(simpleSystem_0_mainBlock_A1_2_startAgent, "execute");
theEngine.clockToEObject.put(simpleSystem_0_mainBlock_A2_1_stopAgent, "//@ownedApplication/@ownedAgents.1");
theEngine.clockToMethodName.put(simpleSystem_0_mainBlock_A2_1_stopAgent, "stop");
theEngine.clockToEObject.put(simpleSystem_0_mainBlock_A1_2_stopAgent, "//@ownedApplication/@ownedAgents.0");
theEngine.clockToMethodName.put(simpleSystem_0_mainBlock_A1_2_stopAgent, "stop");
theEngine.clockToEObject.put(simpleSystem_0_mainBlock_A2_1_isExecuting, "//@ownedApplication/@ownedAgents.1");
theEngine.clockToMethodName.put(simpleSystem_0_mainBlock_A2_1_isExecuting, "isExecuting");
theEngine.clockToEObject.put(simpleSystem_0_mainBlock_A1_2_isExecuting, "//@ownedApplication/@ownedAgents.0");
theEngine.clockToMethodName.put(simpleSystem_0_mainBlock_A1_2_isExecuting, "isExecuting");
theEngine.clockToEObject.put(simpleSystem_0_mainBlock_pA1inState_3_read, "//@ownedApplication/@ownedAgents.0/@ownedPorts.1");
theEngine.clockToMethodName.put(simpleSystem_0_mainBlock_pA1inState_3_read, "read");
theEngine.clockToEObject.put(simpleSystem_0_mainBlock_pA2in_4_read, "//@ownedApplication/@ownedAgents.1/@ownedPorts.0");
theEngine.clockToMethodName.put(simpleSystem_0_mainBlock_pA2in_4_read, "read");
theEngine.clockToEObject.put(simpleSystem_0_mainBlock_pA1out_5_write, "//@ownedApplication/@ownedAgents.0/@ownedPorts.0");
theEngine.clockToMethodName.put(simpleSystem_0_mainBlock_pA1out_5_write, "write");
theEngine.clockToEObject.put(simpleSystem_0_mainBlock_pA1outState_6_write, "//@ownedApplication/@ownedAgents.0/@ownedPorts.2");
theEngine.clockToMethodName.put(simpleSystem_0_mainBlock_pA1outState_6_write, "write");
theEngine.clockToEObject.put(simpleSystem_0_mainBlock_place1_7_push, "//@ownedApplication/@ownedPlaces.0");
theEngine.clockToMethodName.put(simpleSystem_0_mainBlock_place1_7_push, "push");
theEngine.clockToEObject.put(simpleSystem_0_mainBlock_placeA1State_8_push, "//@ownedApplication/@ownedPlaces.1");
theEngine.clockToMethodName.put(simpleSystem_0_mainBlock_placeA1State_8_push, "push");
theEngine.clockToEObject.put(simpleSystem_0_mainBlock_place1_7_pop, "//@ownedApplication/@ownedPlaces.0");
theEngine.clockToMethodName.put(simpleSystem_0_mainBlock_place1_7_pop, "pop");
theEngine.clockToEObject.put(simpleSystem_0_mainBlock_placeA1State_8_pop, "//@ownedApplication/@ownedPlaces.1");
theEngine.clockToMethodName.put(simpleSystem_0_mainBlock_placeA1State_8_pop, "pop");
theEngine.clockToEObject.put(simpleSystem_0_mainBlock_CPU1_9_isExecuting, "//@ownedHWPlatform/@ownedHWResources.0");
theEngine.clockToMethodName.put(simpleSystem_0_mainBlock_CPU1_9_isExecuting, "incCycle");



	org.eclipse.emf.ecore.resource.ResourceSet resourceSet = new org.eclipse.emf.ecore.resource.impl.ResourceSetImpl();

	resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put(
			"sigpml", new org.eclipse.emf.ecore.xmi.impl.EcoreResourceFactoryImpl());

	// Register the package -- only needed for stand-alone!
	 org.eclipse.gemoc.example.moccmlsigpml.model.sigpml.SigpmlPackage sigpmlpackage =  org.eclipse.gemoc.example.moccmlsigpml.model.sigpml.SigpmlPackage.eINSTANCE;

RegularFile model = new RegularFile("Simple.sigpml");
	try {
		JavaResource.exportToFile("/Simple.sigpml", model);
	} catch (java.io.IOException e) {
		e.printStackTrace();
	}	org.eclipse.emf.ecore.resource.Resource resource = resourceSet.getResource(org.eclipse.emf.common.util.URI.createFileURI("Simple.sigpml"), true);
	
	org.eclipse.gemoc.example.moccmlsigpml.model.sigpml.impl.SystemImpl root = (org.eclipse.gemoc.example.moccmlsigpml.model.sigpml.impl.SystemImpl) resource.getContents().get(0);
	ArrayList<Object> param = new ArrayList<Object>();
	param.add(new org.eclipse.emf.ecore.util.BasicInternalEList<String>(String.class));
	theEngine.execute(root, "initializeModel", param);

	theEngine.populateVariableMapFromAnnotations(root);

    if(rtdCouples.size() > 0) {
    	theEngine.initializeCSV(rtdCouples, root);
		theEngine.generatePlotScript(rtdCouples, root);
    }

   	Solver solver = new Solver(allClocks, allConstraintsButExclusions, allExclusions);
   	BlockingQueue<Exception> exceptionQueue = new ArrayBlockingQueue<>(1);
   
   	
   	Thread solverThread = theEngine.startSolverInThread(exceptionQueue, solver, nbSteps, logStepsOnly, logClocks, rtdCouples, resource, root);
    solverThread.start();
    ServerSocket server = null;
 try {
	server = new ServerSocket(39635);
	
	Socket clientSocket = server.accept();
	
	ObjectInputStream cin = new ObjectInputStream(clientSocket.getInputStream());
	ObjectOutputStream cout = new ObjectOutputStream(clientSocket.getOutputStream());
	
	Object clientCommand = cin.readObject();
	while(!theEngine.simulationEnded.tryAcquire() || clientCommand instanceof StopCommand) { 
		System.out.println("Command received: "+clientCommand);
		if(clientCommand instanceof DoStepCommand) {
			theEngine.timeBeforeDoStep = theEngine.lastknownTime.doubleValue();
			System.out.println("DoStep starts @"+theEngine.timeBeforeDoStep);
			theEngine.currentPredicate = ((DoStepCommand) clientCommand).predicate;
			StopCondition stopCond = theEngine.doStep(theEngine.currentPredicate);
			System.out.println("DoStep stops @"+stopCond.timeValue+" due to "+stopCond.stopReason);
			cout.writeObject(stopCond);
		}	
		if(clientCommand instanceof GetVariableCommand) {
			String varQN = ((GetVariableCommand) clientCommand).variableQualifiedName;
			Object varValue= theEngine.getVariable(varQN);
			cout.writeObject(varValue);
		}
		if(clientCommand instanceof SetVariableCommand) {
			String varQN = ((SetVariableCommand) clientCommand).variableQualifiedName;
			Object newValue = ((SetVariableCommand) clientCommand).newValue; 
			Boolean res = theEngine.setVariable(varQN, newValue);
			cout.writeObject(res);
		}
		System.out.println("wait for a new command.");
		clientCommand = cin.readObject();
	}
    server.close();
	solverThread.join();
 	} catch (IOException | InterruptedException | ClassNotFoundException e) {
	 e.printStackTrace();
	 }
    
    
    model.delete();    
    System.out.println();
    System.out.println("/**********************\n");
    System.out.println("*     simulation ends      *\n");
    System.out.println("**********************/");
    System.out.println();	
    return;
    }
	
	Semaphore startDoStepSemaphore  = new Semaphore(0);
	Semaphore finishDoStepSemaphore = new Semaphore(0);
	Semaphore simulationEnded       = new Semaphore(0);
	Map<String, Object> qualifiedNameToEObject = new HashMap<>(); //initialized in main, used in GetVariable
	Map<String, Method> qualifiedNameToMethod = new HashMap<>(); 
	CoordinationPredicate currentPredicate;
	AtomicDouble lastknownTime = new AtomicDouble(-1.0);
	double timeBeforeDoStep = -1;
	StopCondition lastStopcondition = null;
	Object timeObject = null;
	Class<?> timeType = null;

	/**
	 * this method go through all the model and its aspects and populate the {@link #qualifiedNameToEObject} map with Variables annotated as @Input or @Output
	 */
	private void populateVariableMapFromAnnotations(EObject root) {
		TreeIterator<EObject> contentIt = root.eAllContents();
		while(contentIt.hasNext()) {
			EObject eo = contentIt.next();
//			List<Class<?>> aspects = K3DslHelper.getAspectsOn(languageName, eo.getClass());
			for(Class<?> aspect : savedAspects) {
				try {
					Class<?> propertyClass = this.getClass().getClassLoader().loadClass(aspect.getTypeName()+aspect.getSimpleName()+"Properties");
					for (Field f : propertyClass.getFields()) {
						for (Method m : aspect.getMethods()) {
							if (m.getName().compareTo(f.getName()) == 0) {
								if (m.isAnnotationPresent(Input.class)) {
									qualifiedNameToEObject.putIfAbsent(EcoreQNHelper.getQualifiedName(eo)+"::"+f.getName(), eo);
									if (m.getParameterCount() == 2) {
										qualifiedNameToMethod.putIfAbsent(EcoreQNHelper.getQualifiedName(eo)+"::"+f.getName()+"::set", m);
									}
								}
								if (m.isAnnotationPresent(Output.class)) {
									qualifiedNameToEObject.putIfAbsent(EcoreQNHelper.getQualifiedName(eo)+"::"+f.getName(), eo);
									if (m.getParameterCount() == 1) {
										qualifiedNameToMethod.putIfAbsent(EcoreQNHelper.getQualifiedName(eo)+"::"+f.getName()+"::get", m);
									}
								}
								if (m.isAnnotationPresent(Time.class)) {
									try {
										timeObject = f.get(eo); 
										timeType = f.getType();
									} catch (IllegalArgumentException | IllegalAccessException e) {
										e.printStackTrace();
									}
								}
							}
						}
					}
				} catch (ClassNotFoundException | SecurityException e1) {
					e1.printStackTrace();
				}
			}
		}
			
	}
	
	public Object getVariable(String varQN) {
		Object var = qualifiedNameToEObject.get(varQN);
		Method method = qualifiedNameToMethod.get(varQN+"::get");
		if (var == null || method == null) {
			return false;
		}
		try {
			StepManagerRegistry.getInstance().unregisterManager(this);
			Object res = method.invoke(null, var);
			StepManagerRegistry.getInstance().registerManager(this);
			return res;
		} catch (SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			e.printStackTrace();
		}
		
		return true;
	}
	
	public boolean setVariable(String varQN, Object newValue) {
		Object var = qualifiedNameToEObject.get(varQN);
		Method method = qualifiedNameToMethod.get(varQN+"::set");
		if (var == null || method == null) {
			return false;
		}
		try {
			StepManagerRegistry.getInstance().unregisterManager(this);
			method.invoke(null, var, newValue);
			StepManagerRegistry.getInstance().registerManager(this);
		} catch (SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			e.printStackTrace();
		}
		
		return true;
	}
	
	private void decrement(Semaphore sem) {
		try {
			sem.acquire();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	private void increment(Semaphore sem) {
			sem.release();
	}
	
	
	public StopCondition doStep(CoordinationPredicate predicate) {
		increment(startDoStepSemaphore);
		decrement(finishDoStepSemaphore);
		return lastStopcondition;
		//should do something Here according to what has been done during the step (according to the stopCondition)
	}
	
	public Thread startSolverInThread(BlockingQueue<Exception> exceptionQueue, Solver solver, int nbSteps, boolean logStepsOnly, 
									  Boolean logClocks, ArrayList<ArrayList<String>> rtdCouples, org.eclipse.emf.ecore.resource.Resource resource,
									  EObject root) {
		return new Thread(() ->
		// since aspect's methods are static, first arg is null
		{
			try {
				decrement(startDoStepSemaphore);
				for (int currentStep = 0; currentStep < nbSteps; currentStep++) {
					if (logStepsOnly || logClocks)
						System.out.println("-------------step " + currentStep + "\n");

					solver.doStep();
					
					checkLogicalStepStopPredicate();
					checkEventStopPredicate(solver, resource);

					this.executeDSAsAndLog(logClocks, clockToMethodName, clockToEObject, resource, solver);
	
					if (rtdCouples.size() > 0) {
						this.logRTDs(rtdCouples, root);
					}
				}

			} catch (/* IllegalAccessException | */IllegalArgumentException /* | InvocationTargetException */ e) {
				exceptionQueue.offer(e);
			}
			increment(simulationEnded);
		});
	}


	private void checkLogicalStepStopPredicate() {
		if (this.currentPredicate != null
			&& this.currentPredicate.contains(StopReason.LOGICALSTEP,null, null, null))
		{
			this.currentPredicate.getLogicalStepPredicate().nbStepsRequired--;
			if(this.currentPredicate.getLogicalStepPredicate().nbStepsRequired == 0) {
				// struct to communicate the stop condition to the server
				lastStopcondition = new StopCondition(StopReason.LOGICALSTEP, null, null, lastknownTime.floatValue()); // always lastKnownTime ?
				increment(finishDoStepSemaphore); // give control back to the caller.
				// wait for stuff to be done in the server before to continue
				decrement(startDoStepSemaphore); // waiting for the caller to do something
			}
		}
	}


	private void checkEventStopPredicate(Solver solver, Resource resource) {
		if (this.currentPredicate != null) {
			for (Clock pt_c : solver.clocks) {
				String uriFragment = clockToEObject.get(pt_c);
				String supportQN = "";
				if (uriFragment != null) {
					EObject support = resource.getEObject(uriFragment);
					supportQN = EcoreQNHelper.getQualifiedName(support);
				}
				if (pt_c.status == QuantumBoolean.TRUE && this.currentPredicate.contains(StopReason.EVENT,null, supportQN, pt_c.name)) {
					// struct to communicate the stop condition to the server
					lastStopcondition = new StopCondition(StopReason.EVENT, clockToEObject.get(pt_c),
							pt_c.name, lastknownTime.floatValue()); // always lastKnownTime ?
					increment(finishDoStepSemaphore); // give control back to the caller.
					// wait for stuff to be done in the server before to continue
					decrement(startDoStepSemaphore); // waiting for the caller to do something
				}
			}
		}
	}
	
	
	public void executeDSAsAndLog(boolean logClocks, Map<Clock, String> clockToMethodName, Map<Clock, String> clockToEObject,
			org.eclipse.emf.ecore.resource.Resource resource, Solver solver) {
		StepManagerRegistry.getInstance().registerManager(this);
        for(Clock pt_c: solver.clocks) {
            if (! (pt_c instanceof Constraint)) {
               if (logClocks) System.out.println(pt_c);
            	if (pt_c.status == QuantumBoolean.TRUE) {
            		String eobjectURIFrag = clockToEObject.get(pt_c);
            		if (eobjectURIFrag != null) {
            			org.eclipse.emf.ecore.EObject eo = resource.getEObject(eobjectURIFrag);
            			this.execute(eo, clockToMethodName.get(pt_c), null);
            		}
            	}
            }
        }
        StepManagerRegistry.getInstance().unregisterManager(this);
	}

	@Override
	public void readyToRead(EObject caller, Object propContainer, StepCommand command, String className, String propertyName) {
		System.out.println("in readyToRead("+propertyName+") on "+EcoreQNHelper.getQualifiedName(caller));
		if(currentPredicate != null && currentPredicate.contains(StopReason.READYTOREAD ,caller,className,propertyName)) {
			//struct to communicate the stop condition to the server
			lastStopcondition = new StopCondition(StopReason.READYTOREAD, /*caller, propContainer, command,*/ EcoreQNHelper.getQualifiedName(caller), propertyName, lastknownTime.floatValue()); //always lastKnownTime ?
			increment(finishDoStepSemaphore); // give control back to the caller.
			//wait for stuff to be done in the server before to continue
			decrement(startDoStepSemaphore); //waiting for the caller to do something
		}
		command.execute();
	}

	

	@Override
	public void justUpdated(EObject caller, Object propContainer, StepCommand command, String className, String propertyName) {
		System.out.println("in justUpdated("+propertyName+") on "+EcoreQNHelper.getQualifiedName(caller));
		command.execute();
		if(currentPredicate != null && currentPredicate.contains(StopReason.UPDATE ,caller,className,propertyName)) {
			//struct to communicate the stop condition to the server
			lastStopcondition = new StopCondition(StopReason.UPDATE, /*caller, propContainer, command,*/ EcoreQNHelper.getQualifiedName(caller), propertyName, lastknownTime.floatValue()); //always lastKnownTime ?
			increment(finishDoStepSemaphore); // give control back to the caller.
			//Optionally waiting for the caller to do something before to continue (logically we wait for antoher doStep)
			decrement(startDoStepSemaphore); 
		}
	}

	@Override
	public void timePassed(EObject caller, Object propContainer, StepCommand command, String className, double value) {
		System.out.println("in timePassed("+value+") on "+EcoreQNHelper.getQualifiedName(caller));
		command.execute();
		lastknownTime.set(value);
		
		if (currentPredicate != null && currentPredicate.contains(StopReason.TIME ,caller,className, Double.toString(value))) {
			double delta = ((TemporalPredicate)currentPredicate.getTemporalPredicate()).deltaT;
			if ((value - timeBeforeDoStep) >= delta){
				//struct to communicate the stop condition to the server
				lastStopcondition = new StopCondition(StopReason.TIME, /*caller, propContainer, command, */EcoreQNHelper.getQualifiedName(caller), "time", lastknownTime.floatValue()); //always lastKnownTime ?
				increment(finishDoStepSemaphore); // give control back to the caller.
				//wait for stuff to be done in the server before to continue
				decrement(startDoStepSemaphore); //waiting for the caller to do something
			}
		}
	}


	@Override
	/*
	 * This is the operation called from K3 code. We use this callback to pass the
	 * command to the generic executeOperation operation. (non-Javadoc)
	 * 
	 * @see fr.inria.diverse.k3.al.annotationprocessor.stepmanager.IStepManager#
	 * executeStep(java.lang.Object,
	 * fr.inria.diverse.k3.al.annotationprocessor.stepmanager.StepCommand,
	 * java.lang.String)
	 */
	public void executeStep(final Object caller, final StepCommand command, String className, final String methodName) {
		System.out.println("in executeDebugStep in "+className+"::"+methodName+" on "+caller);
		if (currentPredicate != null && currentPredicate.contains(StopReason.DEBUGSTEP, null ,null , null)) {
				//struct to communicate the stop condition to the server
				lastStopcondition = new StopCondition(StopReason.DEBUGSTEP, caller.toString(), "time", lastknownTime.floatValue()); //always lastKnownTime ?
				increment(finishDoStepSemaphore); // give control back to the caller.
				//wait for stuff to be done in the server before to continue
				decrement(startDoStepSemaphore); //waiting for the caller to do something
				command.execute();
		}
	}

	
	
	//unused from inheritance....



	@Override
	protected void executeEntryPoint() {
		// TODO Auto-generated method stub
		
	}



	@Override
	protected void initializeModel() {
		// TODO Auto-generated method stub
		
	}



	@Override
	protected void prepareEntryPoint(GenericModelExecutionContext<ISequentialRunConfiguration> arg0) {
		// TODO Auto-generated method stub
		
	}



	@Override
	protected void prepareInitializeModel(GenericModelExecutionContext<ISequentialRunConfiguration> arg0) {
		// TODO Auto-generated method stub
		
	}
}
