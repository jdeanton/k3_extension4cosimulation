package fr.inria.kairos.coordination.mock;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.gemoc.execution.commons.commands.DoStepCommand;
import org.eclipse.gemoc.execution.commons.commands.GetVariableCommand;
import org.eclipse.gemoc.execution.commons.commands.SetVariableCommand;
import org.eclipse.gemoc.execution.commons.commands.StopCommand;
import org.eclipse.gemoc.execution.commons.commands.StopCondition;
import org.eclipse.gemoc.execution.commons.predicates.EventPredicate;
import org.eclipse.gemoc.execution.commons.predicates.LogicalStepPredicate;
import org.eclipse.gemoc.execution.commons.predicates.ReadyToReadPredicate;
import org.eclipse.gemoc.execution.commons.predicates.TemporalPredicate;
import org.eclipse.gemoc.execution.commons.predicates.UpgradedPredicate;


public class TestCoordination1 {

	public static void main(String[] args) throws UnknownHostException, IOException, ClassNotFoundException {
		Socket socket = new Socket("localhost", 39635);
		
		ObjectOutputStream cout = new ObjectOutputStream(socket.getOutputStream());
		ObjectInputStream  cin  = new ObjectInputStream(socket.getInputStream());
		
		
		
		System.out.println("ready to coordinate !");
		
		Scanner keyboard = new Scanner(System.in);
		System.out.println("enter an integer");
		int myint = keyboard.nextInt();
		while(myint != 42) {
			if (myint == 0){
				UpgradedPredicate p = new UpgradedPredicate("currentValue", "CPUprotection::cpuTemperature");
				DoStepCommand doStep = new DoStepCommand(p);
				System.out.println("ask for doStep(update(CPUprotection::cpuTemperature::currentValue))"); //never end since the model does not modify this variable
				cout.writeObject(doStep);
				StopCondition sc = (StopCondition) cin.readObject();
			
				System.out.println(sc.stopReason+" "+sc.propertyName+" @ "+sc.timeValue);
			}else
			if (myint == 1) {
				ReadyToReadPredicate p = new ReadyToReadPredicate("currentValue", "CPUprotection::cpuTemperature");
				DoStepCommand doStep = new DoStepCommand(p);
				System.out.println("ask for doStep(read(CPUprotection::cpuTemperature::currentValue))");
				cout.writeObject(doStep);
				StopCondition sc = (StopCondition) cin.readObject();
			
				System.out.println(sc.stopReason+" "+sc.propertyName+" @ "+sc.timeValue);
			}
			else
			if (myint == 2) {
				String varQN = "CPUprotection::cpuTemperature::currentValue";
				GetVariableCommand getVar = new GetVariableCommand(varQN);
				System.out.println("ask for getVariable");
				
				cout.writeObject(getVar);
				Object varValue = (Object) cin.readObject();	
			
				System.out.println("value of "+varQN+" is "+varValue);
			}	
			else
			if (myint == 3) {
				String varQN = "CPUprotection::cpuTemperature::currentValue";
				System.out.println("please enter the value to set");
				int newValue = keyboard.nextInt();
				SetVariableCommand setVar = new SetVariableCommand(varQN, new Integer(newValue));
				System.out.println("ask for setVariable to a fifo with "+newValue);
				
				cout.writeObject(setVar);
				Boolean resValue = (Boolean) cin.readObject();
			
				System.out.println("value  is set correctly ?: "+resValue);
			}else
			if (myint == 4) {
				LogicalStepPredicate p = new LogicalStepPredicate(80);
				DoStepCommand doStep = new DoStepCommand(p);
				System.out.println("ask for doStep(80 logicalSteps)");
				cout.writeObject(doStep);
				StopCondition sc = (StopCondition) cin.readObject();
				System.out.println(sc.stopReason+" "+sc.propertyName+" @ "+sc.timeValue);
			}else
			if (myint == 5) {
				EventPredicate p = new EventPredicate("occurs", "CPUprotection::switchCPUState");
				DoStepCommand doStep = new DoStepCommand(p);
				System.out.println("ask for doStep(ticks(CPUprotection::switchCPUState::occurs))");
				cout.writeObject(doStep);
				StopCondition sc = (StopCondition) cin.readObject();
				System.out.println(sc.stopReason+" "+sc.propertyName+" @ "+sc.timeValue);
			}else	
			if (myint == 6) {
				TemporalPredicate p = new TemporalPredicate(10);
				DoStepCommand doStep = new DoStepCommand(p);
				System.out.println("ask for doStep(10s)");
				cout.writeObject(doStep);
				StopCondition sc = (StopCondition) cin.readObject();
				System.out.println(sc.stopReason+" "+sc.propertyName+" @ "+sc.timeValue);
			}else	
			if (myint == 88) {
				ReadyToReadPredicate p = new ReadyToReadPredicate("notExisting", "justRun");
				DoStepCommand doStep = new DoStepCommand(p);
				System.out.println("ask for doStep infinite");
				cout.writeObject(doStep);
				StopCondition sc = (StopCondition) cin.readObject();
			
				System.out.println(sc.stopReason+" "+sc.propertyName+" @ "+sc.timeValue);
			}
			myint = keyboard.nextInt();
		}
		StopCommand stop = new StopCommand();
		cout.writeObject(stop);
		keyboard.close();
		socket.close();
	}

}
